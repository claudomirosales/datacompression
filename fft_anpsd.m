function [ ANPSD ] = fft_anpsd( data, varargin )
%ANPSD Estima o ANPSD dos dados fornecidos.
%
%ENTRADA:
%   data: Medições.
%   (OPCIONAL) dt : Taxa de amostragem. Valor padrão: 0.01.

%% Verificação dos argumentos de entrada.
% No máximo 1 argumento opcional.
numvarargs = length(varargin);
if numvarargs > 1
    error('Máximo de 1 argumento opcional permitido.');
end

% Valores default dos argumentos opcionais.
% salvar; outFile; home.
optargs = {0.01};

% Sobrescreve os valores default com os especificados.
optargs(1:numvarargs) = varargin;

% Valores finais das variáveis opcionais.
[dt] = optargs{:};


% dt data increment in seconds
fs=1/dt; % frequency

%% Application of a filter
% Design a Butterworth IIR digital filter
% For data sampled at 100 Hz, design a 3th-order highpass Butterworth
% filter, with a cutoff frequency of 3 Hz, which corresponds to a
% normalized value of 0.06.
wn = 2/(fs/2); % normalized cutoff frequency Wn (2 Hz)
[bb,aa] = butter(3,wn,'high');

% Performs zero-phase digital filtering by processing the input data
data = filtfilt(bb,aa,data);
clear aa bb;

%% Estimate the FFT
% Hanning window
[xfreq, Yfft] = fct_fft(data,dt);

m=length(xfreq);

% Auto-power spectral density
psd=1/(m*dt)*Yfft.*conj(Yfft);

%% Averaged normalized power spectral density - ANPSD
% Estimate ANPSD
mean_vector=sum(psd);
NPSD=psd./repmat(mean_vector,[m 1]);
ANPSD=sum(NPSD,2);

end

