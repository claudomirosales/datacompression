function [feature_vector]=feature_shm_ufpa(data,dt,limits)

% dt data increment in seconds
fs=1/dt; % frequency

%n=length(data);

%time=0:dt:dt*(n-1); time=time';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot the raw data

%----
% figure
% plot(data,'k')
% title('Unfiltered Acceleration Data')
% xlabel('Data Points')
% ylabel('Aceleração (g)')
% grid on
%----

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Application of a filter

% Design a Butterworth IIR digital filter
% For data sampled at 100 Hz, design a 3th-order highpass Butterworth
% filter, with a cutoff frequency of 3 Hz, which corresponds to a
% normalized value of 0.06.
wn = 2/(fs/2); % normalized cutoff frequency Wn (2 Hz)
[bb,aa] = butter(3,wn,'high');
% Performs zero-phase digital filtering by processing the input data
data = filtfilt(bb,aa,data); clear aa bb

%----
% figure
% plot(data,'b');
% title('Filtered Data')
% xlabel('Data Points')
% ylabel('Aceleração (g)')
% grid on
%----

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Estimate the FFT

% Hanning window
[xfreq, Yfft] = fct_fft(data,dt);

m=length(xfreq);  df=xfreq(2)-xfreq(1);

% Auto-power spectral density
psd=1/(m*dt)*Yfft.*conj(Yfft);

%----
% figure
% plot(xfreq,abs(psd))
% title('Power Spectrum')
% xlabel('Frequency (Hz)');
% ylabel('Energy');
%----


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Averaged normalized power spectral density - ANPSD

% Estimate ANPSD

mean_vector=sum(psd);
NPSD=psd./repmat(mean_vector,[m 1]);
ANPSD=sum(NPSD, 2);

%----
% figure
% plot(xfreq,abs(ANPSD))
% title('ANPSD')
% xlabel('Frequency (Hz)');
% ylabel('Energy');
%----

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extract the natural frequencies

a1=limits(1,1); d1=limits(1,2);

%----
% figure
% plot(xfreq,abs(ANPSD))
% title('ANPSD')
% line('XData',[xfreq(a1-d1) xfreq(a1-d1)],'YData',[0 max(abs(ANPSD))],'Color','k','LineWidth',1,'LineStyle','-.')
% line('XData',[xfreq(a1+d1) xfreq(a1+d1)],'YData',[0 max(abs(ANPSD))],'Color','k','LineWidth',1,'LineStyle','-.')
% xlabel('Frequency (Hz)');
% ylabel('Energy');
%----

[a,indx]=max(abs(ANPSD(a1-d1:a1+d1))); ind=a1-d1+indx-1; freq1=xfreq(ind);

a1=limits(2,1); d1=limits(2,2);

%----
% figure
% plot(xfreq,abs(ANPSD))
% title('ANPSD')
% line('XData',[xfreq(a1-d1) xfreq(a1-d1)],'YData',[0 max(abs(ANPSD))],'Color','k','LineWidth',1,'LineStyle','-.')
% line('XData',[xfreq(a1+d1) xfreq(a1+d1)],'YData',[0 max(abs(ANPSD))],'Color','k','LineWidth',1,'LineStyle','-.')
% xlabel('Frequency (Hz)');
% ylabel('Energy');
%----

[a,indx]=max(abs(ANPSD(a1-d1:a1+d1))); ind=a1-d1+indx-1; freq2=xfreq(ind);

% Energy
%energy=sum(abs(ANPSD(round(end/2):end))*df);
energy = trapz(abs(ANPSD(round(end/2):end)));

feature_vector=[freq1 freq2 energy];

% if isnan(energy) == 1
%     keyboard;
% end

end

