function [comp_data coef] = pwlh(orig_data, epsilon)
% PWLH Compressão de dados usando Piecewise Constant Linear Histogram.
%   Este código implementa o algoritmo de compressão de dados numéricos PWLH.
%   Ajusta a janela de dados de acordo o valor de erro máximo utilizado. 
%   O valor de erro máximo utilizado é determinado através do desvio padrão 
%   de todo o conjunto de dados.
% 
%ENTRADA:
%   orig_data: Vetor coluna com os dados originais.
%   epsilon: Limiar de compressão.
%
%SAÍDA:
%   comp_data: Dados comprimidos.
%   coef: Coeficientes da equação de 1º grau utilizados para aproximar os
%   dados.
%


%% Configuração de Variáveis

% Se o array de dados só possui as medições, então criamos as ID's.
if(size(orig_data,2) == 1)
   orig_data = [ (1:length(orig_data))' orig_data]; 
end

% Os dados são comprimidos quando Max - Min <= 2e.
epsilon = 2 * epsilon;

% Início da janela de compressão.
start_window = 1;

% Matrix com os dados comprimidos. Ele tem o mesmo tamanho da matriz
% original para que aumente o desempenho (segundo o Matlab). No final, nós
% cortamos o que não é necessário.
comp_data = ones(size(orig_data));
% Índice com a linha atual da matriz dos dados comprimidos.
idx_comp = 1;

% Coeficientes da função de 1º grau usados na aproximação.
coef = ones(size(orig_data));
% Índice com a linha atual da matriz dos coeficientes.
idx_coef = 1;

% Tamanho dos dados originais.
tam = length(orig_data);

%% Algoritmo        
while start_window <= tam
        
    % Estamos na última posição e, logo, não há como haver mais janelas.
    % Então salvamos o último ponto e encerramos a compressão.
    if start_window == tam
        comp_data(idx_comp,:) = [orig_data(start_window,1) orig_data(start_window,2)];
        break;
    end
        
    % Tamanho inicial da janela.
    window = 1;
    
    % Fim da janela de compressão.
    end_window = start_window + window;

    % Diferença inicial.
    Max = max(orig_data(start_window:end_window, 2));
    Min = min(orig_data(start_window:end_window, 2));
    dif = Max - Min;
    
    % Variáveis auxiliares.
    dif_ahead = dif;
    window_ahead = window;
    end_window_ahead = end_window;

    % Enquanto a diferença for menor ou igual que o threshold, então
    % aumenta o tamanho da janela.
    while (dif_ahead < epsilon) && (end_window_ahead < tam - 1)
        % Aceita os valores úteis da janela anterior pois não passaram o limiar.
        window = window_ahead;
        end_window = end_window_ahead;
        
        % Calcula a diferença da janela atual.
        Max_ahead = max(orig_data(start_window:end_window, 2));
        Min_ahead = min(orig_data(start_window:end_window, 2));
        dif_ahead = Max_ahead - Min_ahead;
        
        % Aumenta a janela.
        window_ahead = window_ahead + 1;

        % Atualiza o limite final da janela.
        end_window_ahead = start_window + window_ahead;
    end

    % Retira o excedente do limite final, caso tenhamos passado.
    if end_window > tam
        end_window = tam;
    end
    
    % Flag indicando se houve (1) ou não (0) compressão.
    comprimiu = 0;
    
    %% Compressão    
    % Agora que temos uma janela cujos valores estão abaixo do threshold,
    % vamos aproximar os dados.
    if window > 1
        % Armazena o primeiro dado da janela.
        comp_data(idx_comp,:) = [orig_data(start_window,1) orig_data(start_window,2)];
        % Próxima posição disponível.
        idx_comp = idx_comp + 1;
        
        % Armazena o último dado da janela.
        comp_data(idx_comp,:) = [orig_data(end_window,1) orig_data(end_window,2)];
        % Próxima posição disponível.
        idx_comp = idx_comp + 1;
        
        % Aproxima linearmente a janela de dados por um polinômio do
        % primeiro grau
        x = orig_data(start_window:end_window, 1);
        y = orig_data(start_window:end_window, 2);
        coef(idx_coef,:) = polyfit(x, y, 1);
        % Próxima posição disponível.
        idx_coef = idx_coef + 1;
                
        comprimiu = 1;
    else
        % Janela de tamanho 1. Se isso ocorreu é porque passamos o threshold
        % e temos que preservar os dados.
        % Salva apenas a primeira amostra da janela
        comp_data(idx_comp,:) = [orig_data(start_window,1) orig_data(start_window,2)];
        % Próxima posição disponível.
        idx_comp = idx_comp + 1;
    end
            
    % Avança a janela
    if comprimiu == 1
        % Como comprimimos, já armazenamos o início e o fim da janela.
        % Então descartamos o fim.
        start_window = end_window + 1;
    else
        % Quando não comprimimos, só armazenamos o início da janela, por
        % isso vamos começar pelo fim.
        start_window = start_window + 1;
    end

end

% Só vamos usar até onde inserimos dados comprimidos. O resto é descartado.
comp_data = comp_data(1:idx_comp, :);
coef = coef(1:idx_coef, :);

end