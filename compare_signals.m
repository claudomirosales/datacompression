% Calcula o RMSE de todo o sinal original e todo o sinal aproximado.

addpath('./utils/');

clc
clear all
close all

% Separador de arquivos da plataforma.
f = filesep;

% Diretório onde estão os sinais aproximados.
home = 'sinal_aproximado_normalizado';

% Utilizar os sinais aproximados deste algoritmo.
alg = 'cheb';

%% Exemplo de trecho
% plotOrigAprox(sinal_original_completo(6472000:6472900,1), sinal_aproximado_completo(6472000:6472900,1));

%% Carrega a série temporal

% Usaremos todas as leituras feitas em uma determinada hora.
dirName = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';
hora='12'; % Hora do monitoramento. Valores possíveis 00-23.
pattern=strcat('*_',hora,'.mat'); % Padrão para buscar leituras de uma determinada hora.
files = dir( fullfile(dirName, pattern) );
files = {files.name}'; % Todos os arquivos .mat com as leituras da hora.


%% Monta todo o sinal original
observ = numel(files);
canal = 2;
tam = 65536;
sinal_original_completo = zeros(tam*observ,1);

for t=1:observ
    % Caminho completo do arquivo .mat.
    fname = fullfile(dirName,files{t});
    % Carrega os dados como struct.
    tmp = load(fname);
    % Obtém apenas a matriz desta observação com os 8 canais.
    dataset = tmp.data;
    dataset(:,canal) = normalize_var(dataset(:,canal), -1, 1);
    
    ini = 1 + (tam * t - tam);
    fim = tam * t;
        
    sinal_original_completo(ini:fim,1) = dataset(:,canal);
 
end



%% Monta todo o sinal aproximado
%Primeira coluna: k; Segunda coluna: nrmse's.
erros_nrmse = zeros(10,2);
erros_nrmse(:,1) = 0.1:0.1:1.0;
ind = 1; %índice do array dos erros.

% Pra comparar o sinal completo de um k, só remover a sequência.
for k=5.0
    
    fprintf('Carregando pasta %.1f\n', k);
    
    pasta = strcat(home,f,alg,f,num2str(k));
    files = dir( fullfile(pasta, 'ID_*') );    
    files = {files.name}';

    observ = numel(files);
    canal = 1;
    tam = 65536;
    sinal_aproximado_completo = zeros(tam * observ,1);

    for t=1:observ
        % Caminho completo do arquivo .mat.
        fname = fullfile(pasta,files{t});
        % Obtém apenas a matriz desta observação com os 8 canais.
        dataset = abrir(fname,[tam,1]);

        ini = 1 + (tam * t - tam);
        fim = tam * t;

        sinal_aproximado_completo(ini:fim,1) = dataset(:,canal);
    end
    
    % Agora, com todo o sinal aproximado, tiramos o NRMSE.
    erro = nrmse(sinal_original_completo, sinal_aproximado_completo);
    erros_nrmse(ind,2) = erro;
    ind = ind + 1;
end

