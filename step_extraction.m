function [ feat_vector ] = step_extraction ( data, col )
%STEP_EXTRACTION Etapa de extração de features.
%   São utilizadas 3 features do sinal para a detecção de danos:
%   As duas primeiras são a Primeira e Terceira Frequência Natural (F1 e
%   F3, respectivamente).
%   A terceira consiste na quantidade de energia presente na segunda metade
%   do espectro.
%   INPUT
%       data: Matrix com as leitura dos acelerômetros no domínio do tempo.
%       col: Coluna da matriz com as leituras desejadas. Nos dados
%       originais, col=2 equivale às leituras do sensor 5. Nos dados
%       comprimidos, normalmente col=1, pois se comprimiu as leituras de
%       apenas um sensor.
%   OUTPUT
%       feat_vector: Feature vector com as 3 features (F1, F3, A).

    % Propriedades de amostragem
    dt=0.01; % segundos
    
    % Janelas para estimar as frequências naturais F1 e F3.
    limits=[2600 300; 6600 900];
    
    feat_vector = feature_shm_ufpa(data(:,col), dt, limits);
end

