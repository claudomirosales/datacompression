function coef = ARP(dados, ordem, pesos)
tam = length(dados);

if isempty(pesos)
    W = eye(tam-ordem);
elseif (length(pesos) == tam-ordem)
    W = diag(pesos);
else
    error('myApp:argChk', 'Inconsistência no comprimento do vetor de ponderação.')
end

Y = dados(:);
X = ones(tam-ordem, ordem+1);
for i = 1:ordem
    X(:, i+1) = Y(i:tam-ordem+i-1);
end

% Coeficientes
coef = (X.'*W*X)\X.'*W*Y(ordem+1:end);

% pred = [Y(end-d+1:end).' ones(1, nPred)];
% for i = 1:nPred
%     pred(d+i) = [1 pred(i:d+i-1)]*p;
% end
% pred = shiftdim(pred(d+1:end), size(y, 2)==1);

end