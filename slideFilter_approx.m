function [ aprox_data ] = slideFilter_approx( comp_data, orig_tam )
%SLIDEFILTER_APPROX Aproximação dos dados comprimidos pelo algoritmo Slide
%Filter.
%   A aproximação funciona de modo a identificar dois pontos nos dados
%   comprimidos, ambos conectados. Feito isso, os pontos da reta são
%   calculados com base nesses dois pontos.
%
%ENTRADA:
%   comp_data: Dados comprimidos pelo Slide Filter.
%   orig_tam: Tamanho do sinal original.
%
%SAÍDA:
%   aprox_data: Dados aproximados. (:, [DADOS]).


aprox_data = zeros(orig_tam, 1);
aprox_idx = 1;

tam = size(comp_data, 1);

position = 1;

id = 1;

lastID = comp_data(tam, 1);

% [ID VALUE CONNT]
sfEntry1 = zeros(1,3);
sfEntry2 = zeros(1,3);

for i=1:lastID
    % Lê os dados comprimidos.
    if i >= id
        sfEntry1(1,:) = comp_data(position,:);
        position = position + 1;
        
        % Conectado.
        if sfEntry1(1,3) == 1
            
            sfEntry2(1,:) = comp_data(position,:);
            
            % volta para segunda leitura.
            if sfEntry2(1,3) == 1
                id = sfEntry2(1,1) - 1;
            else
                id = sfEntry2(1,1);
                position = position + 1;
                % Garante que não vai procurar mais nenhuma outra linha
                % comprimida.
                if id == lastID
                    id = id + 1;
                end
            end
        else %desconexo (ISSO AQUI NUNCA É EXECUTADO ( ._.)
            % [ID VALOR]
            aprox_data(aprox_idx,:) = sfEntry1(1,2);
            break;
        end
        
        % [X Y]
        p1 = [ sfEntry1(1,1) sfEntry1(1,2) ];
        p2 = [ sfEntry2(1,1) sfEntry2(1,2) ];
        % Linha [X1 Y1 X2 Y2]
        l = [p1 p2 calc_slope(p1, p2)];
    end
    
    %Get point on line at each corresponding time
    aprox_data(aprox_idx,:) = getValue(l, i);
    aprox_idx = aprox_idx + 1;
end

end


function [value] = getValue(line, timestamp)
    SLOPE = 5; INTERCEPT = 6;
    value = line(SLOPE) * timestamp + line(INTERCEPT);    
end

function [slope_intercept] = calc_slope(point1, point2)
    X = 1; Y = 2;
    slope = (point2(Y) - point1(Y)) / (point2(X) - point1(X));
    intercept = point1(Y) - slope * point1(X);    
    slope_intercept = [slope intercept];
end
