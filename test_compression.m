%% Teste dos Algoritmos de Compressão
% Script utilizado para testar os algoritmos de compressão.

clc
clear all
close all

addpath('./utils/');

%% Configuração da série temporal
load '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date/data19971201_12.mat';

% % Usaremos todas as leituras feitas em uma determinada hora.
% dirName = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';
% % Hora do monitoramento. Valores possíveis 00-23.
% hora='12';
% % Padrão para buscar os arquivos de uma determinada hora.
% pattern=strcat('*_',hora,'.mat');
% files = dir( fullfile(dirName, pattern) );    
% % Todos os arquivos .mat com as leituras da hora.
% files = {files.name}';
% % Quantidade de arquivos (Ex.: 200 dias/arquivos).
% observ = numel(files);


% Intervalo dos valores de k.
%intervalo = 0.1:0.1:5;
intervalo = 2.5;

% Guarda as taxas de compressão do algoritmo usado.
taxa = zeros(size(intervalo,2),1);
cont = 1;

erro = zeros(size(intervalo,2),1);

% Dados: Leituras do sensor #5
dados = data(:, 2);
dados = normalize_var(dados, -1, 1);

% Quantidade de leituras da matriz original.
t = size(dados, 1);

% Tamanho em bytes do sinal original.
tam_ori = salvar('sinal_original.bin', dados);

% Desvio-padrão.
dp = std(dados);

%% Comparar limiares
% desvios = zeros(234,1);
% for t=1:observ
%     %Caminho completo do arquivo .mat.
%     fname = fullfile(dirName, files{t});
%     %Carrega os dados como struct.
%     tmp = load(fname);
%     %Obtém apenas a matriz desta observação com os 8 canais.
%     dataset = tmp.data;
%     %Obtém as leituras do canal de interesse.
%     dataset = dataset(:, 2);
%     %Normaliza os dados para o intervalo [-1, 1].
%     dataset = normalize_var(dataset, -1, 1);
%     desvios(t) = std(dataset);    
% end
% fprintf('Plotando Gráfico...\n');
% intervalo = [0.1,2.5,5.0];
% cont = 1;
% figure;
% plotStyle = {'k-','b-','r-'};
% legendInfo = cell(size(plotStyle,2), 1);
% for k = intervalo
%     tmp = k * desvios;
%     plot(tmp, plotStyle{cont});
%     legendInfo{cont} = num2str(k);
%     hold on;
%     cont = cont + 1;
% end
% legend(legendInfo,'Location','northeast');
% return;

%% Compressão
for k = intervalo    
    
    % Threshold.
    e = k * dp;
    
    % Compressão.
    fprintf('Comprimindo para k = %.1f\n', k);
    [ comp_data, comp_control, comp_lengths ] = chebyshev(dados, e);
    
    % Aproximação.
    fprintf('\tAproximando...\n');
    [ aprox_data ] = chebyshev_approx(comp_data, comp_control, comp_lengths, t);
    
    % Taxa de compressão. Se um slote de saída não existe, use 0.
    tam_s1 = salvar('sinal_comprimido.bin', comp_data);
    tam_s2 = salvar('sinal_comprimido_control.bin', comp_control);
    tam_s3 = salvar('sinal_comprimido_len.bin', comp_lengths);
    tam_s4 = 0;
    taxa(cont) = 1 - ( (tam_s1 + tam_s2 + tam_s3 + tam_s4) / tam_ori );
    erro(cont) = e;%<<<<
    cont = cont + 1;
end

plotOrigAprox(dados,aprox_data);
% erro = nrmse(dados,aprox_data);