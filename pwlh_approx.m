function [ aprx_data ] = pwlh_approx(comp_data, coef, tam_orig)
%PWLHAPROX Aproximação do sinal com PWLH.
%   Este código realiza a aproximação dos dados comprimidos pelo PWLH.
% 
%ENTRADA:
%   comp_data:  Conjunto de dados já comprimido.
%   coef:       Matriz Nx2 com os coeficientes das funções de 1º grau.
%   tam_orig:   Tamanho dos dados originais (Número de leituras).
%               Caso você não saiba, use 0.
%SAÍDA:
%   aprx_data: Dados aproximados.
%


% Total de linhas na matriz com dados comprimidos.
tam_comp = size(comp_data,1);

% Índice do array dos coeficientes.
j = 1;

% Para pré-alocarmos a matriz temos que ter, preferência, o tamanho do
% sinal original já que a aproximação tem que ter o mesmo número de pontos.
% Caso não seja possível ter esse valor, usamos 10^5.
cortar = 0;
if tam_orig == 0
    tam_orig = 100000;
    cortar = 1; %Agora temos que cortar o array final.
end

% Dados aproximados.
aprx_data = ones(tam_orig,2);

% Índice da matriz dos dados aproximados.
idx_aprx = 0;

for i=1:tam_comp-1
    
    % Antes, vamos para uma posição válida.
    idx_aprx = idx_aprx + 1;
    
    % Diferença entre o ID atual e o próximo.
    Dif = abs(comp_data(i,1)-comp_data(i+1,1));
    
    % Inclui o primeiro ponto do intervalo.
    aprx_data(idx_aprx,:) = comp_data(i,:);
    
    % Se a diferença for maior que 1, então houve uma aproximação das
    % amostras. Isso ocorre porque na compressão, ID's consecutivas indicam
    % que os dados foram preservados, e ID's faltando indicam aproximação.
    if( Dif > 1)
        % Aproxima os valores das ID's que estão faltando.
        for x=comp_data(i,1)+1 : comp_data(i+1,1)-1
            idx_aprx = idx_aprx + 1; % Vai para uma linha vazia.
            % Aproximação linear
            aprox = x * coef(j,1) + coef(j,2);
            aprx_data(idx_aprx, :) = [x aprox];
        end
        % Avança para os próximos coeficientes a serem usados.
        j = j+1;
    end
    
end

% Agora só falta incluir a última leitura
idx_aprx = idx_aprx + 1; % Vai para uma linha vazia.
aprx_data(idx_aprx, :) = comp_data(tam_comp,:);

if cortar == 1
    aprx_data = aprx_data(1:idx_aprx,:);
end

% Remove a coluna de ID's.
aprx_data(:,1) = [];

end