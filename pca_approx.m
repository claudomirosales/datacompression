function aprox_data = pca_approx(compress_data, tam_orig)
%PCA_APROX Geração do sinal aproximado usando o PCA.
%   Realiza a aproximação do sinal comprimido pelo PCA.
%
%ENTRADA:
%	compress_data: Dados comprimidos.
%	tam_orig: Tamanho do sinal original.
%
%SAÍDA:
%	aprox_data: Dados aproximados. (:, [VALOR]).
%


% Tamanho do sinal comprimido.
tam = size(compress_data, 1);

% Sinal aproximado deve ter o mesmo tamanho que o sinal original.
aprox_data = zeros(tam_orig, 1);

% Índice atual do sinal aproximado.
index_aprox = 1;

for i=1:tam

    % Um sinal comprimido: (ID, VALOR).
    comp_datum = compress_data(i,:);

    % Se a diferença entre a ID do sinal comprimido e o sinal aproximado
    % for maior que 1, isto é, não são ID's consecutivas, então há
    % elementos que foram comprimidos.
    if abs(comp_datum(1) - index_aprox) > 1
                
        % Cria o vetor de dados aproximados até a ID comprimida, inclusive.
        tam = comp_datum(1) - index_aprox + 1;
        range_data = zeros(tam, 1);
        
        % Repete o dado atual em todas as posições.
        range_data(:) = comp_datum(2);
        
        % Armazena os dados aproximados.
        aprox_data(index_aprox:comp_datum(1), 1) = range_data;
    else
        % Dado não comprimido. Apenas copia.
        aprox_data(index_aprox, 1) = comp_datum(2);
    end
    
    % Próxima posição vaga no array aproximado.
    index_aprox = comp_datum(1) + 1;

end

end