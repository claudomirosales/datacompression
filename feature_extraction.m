%% Stand-alone Feature Extraction
% Extrai as features do sinal original e aproximado. Sendo que o sinal
% aproximado já está disponível em algum diretório. As features serão
% salvas sobrescrevendo os arquivos já existentes para cada limiar de cada
% algoritmo. Este script é útil caso se deseje alterar o cálculo das
% features, bem como incluir outras.

addpath('./utils/');

clc
clear all
close all

% Separador de arquivos da plataforma.
f = filesep;

% Usaremos todas as leituras feitas em uma determinada hora.
home_orig = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';
hora='12'; % Hora do monitoramento. Valores possíveis 00-23.
pattern=strcat('*_',hora,'.mat'); % Padrão para buscar leituras de uma determinada hora.
files_orig = dir( fullfile(home_orig, pattern) );    
files_orig = {files_orig.name}'; % Todos os arquivos .mat com as leituras da hora.

% Quantas observações foram feitas para determinado horário (Ex.: 200 dias/arquivos).
observ = numel(files_orig);

% Diretório onde se encontram os sinais aproximados de todos os algoritmos.
dirAprox = 'sinal_aproximado_normalizado';

% Qual sensor usaremos para comprimir (há 8 sensores ao todo).
% COLUNA    1   2   3   4   5   6   7   8
% SENSOR    3   5   6   7   10  12  14  16
canal = 2;


% Algoritmos que serão usados.
%alg = {'cheb', 'pca', 'apca', 'pwlh', 'sf'};
alg = {'sf'};
qtdeAlg = size(alg, 2);

for a = 1:qtdeAlg
    % Variamos o threshold para ver como o sinal aproximado fica.
    for threshold=0.1:0.1:5.0
        
        % Feature vectors para o sinal original e aproximado.
        feat_vector_orig = zeros(observ, 3);
        feat_vector_aprx = zeros(observ, 3);
        
        % Arquivos com o sinal aproximado.
        home_aprox = strcat(dirAprox,f,alg{a},f,num2str(threshold));
        files_aprox = dir( fullfile(home_aprox, 'ID_*.bin') );
        files_aprox = {files_aprox.name}';
        
        % Extrai as features do sinal original e aproximado.
        for t=1:observ
            % Status.
            fprintf('%s: Limiar: %.2f. Extraindo features da série %d de %d.\n', alg{a}, threshold, t, observ);
            
            % Caminho completo do arquivo .mat.
            fname = fullfile(home_orig, files_orig{t});
            % Carrega os dados como struct.
            tmp = load(fname);
            % Obtém apenas a matriz desta observação com os 8 canais.
            dataset_orig = tmp.data;
            % Obtém as leituras do canal de interesse.
            dataset_orig = dataset_orig(:, canal);
            % Normalização. (Let's hope this won't break the script).
            dataset_orig = normalize_var(dataset_orig, -1, 1);
            
            % Abre o sinal aproximado.
            fname = fullfile(home_aprox, files_aprox{t});
            dataset_aprox = abrir(fname,[65536,1]);
            
            % VERIFICA SE OS DADOS APROXIMADOS AINDA CONTÉM NAN.
            [row, col] = find(isnan(dataset_aprox));
            if isempty(row) == 0 || isempty(col) == 0
                fprintf('\t(%s) NaN no limiar %.2f. Arquivo: %s\n', alg{a}, threshold, fname);
            end
            
            % Extrai as features do sinal original.
            feat_vector_orig(t,:) = step_extraction(dataset_orig, 1);            
            % Extrai as features do sinal aproximado.
            feat_vector_aprx(t,:) = step_extraction(dataset_aprox, 1);
            
        end
        
        % Salva as features do sinal original.
        arq = strcat(home_aprox,f,'Features_Orig_Hora_',num2str(hora),'_COL_',num2str(canal),'.bin');
        salvar(arq, feat_vector_orig);
        
        % Salva as features do sinal aproximado.
        arq = strcat(home_aprox,f,'Features_Aprox_Hora_',num2str(hora),'_COL_',num2str(canal),'.bin');
        salvar(arq, feat_vector_aprx);
        
    end
    
end


