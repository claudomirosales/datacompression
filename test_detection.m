%% Teste dos Algoritmos de Detecção de Danos
% Script utilizado para testar a detecção de danos.

addpath('./utils/');

clc
clear all
close all

% Separador de arquivos da plataforma.
f = filesep;

% Diretório com os dados comprimidos e features de todos os algoritmos.
dataHome = 'sinal_aproximado_normalizado';

% Diretório onde os resultados serão armazenados.
dataOut = 'results';

% Nome do algoritmo de detecção de danos.
className = 'nlpca';

% Aplicar a detecção de danos sobre as features destes algoritmos.
%algoritmos = {'pca', 'apca', 'pwlh', 'sf', 'cheb'};
algoritmos = {'cheb'};

% Número de execuções do algoritmo para cada detecção de danos.
numExec = 10;

% Limiares de compressão.
intervalo = 2.0;

% 0: Faz a detecção de danos no sinal original apenas uma vez e seus
% resultados estatísticos são copiados para todos os limiares de
% compressão. 1: Faz a detecção de danos para cada limiar (demora mais).
repetirOrig = 0;

% Usar dados originais (Orig) e/ou dados aproximados (Aprox).
modo = {'Orig', 'Aprox'};
%modo = {'Aprox'};

%------

% 0: Ainda não fez a detecção no original. 1: Já fez.
orig_done = 0;

% Esta matriz guarda a média, desvio-padrão e intervalo de confiança (IC95)
% dos seguintes dados:
% Acurácia, precisão, recal, E1, E2.
% A primeira dimensão é para as estatísticas dos dados originais e a
% segunda, para os dados aproximados.
% (:, [ MÉDIA_ACC, DP_ACC, IC_ACC,
%       MÉDIA_PRE, DP_PRE, IC_PRE,
%       MÉDIA_REC, DP_REC, IC_REC,
%       MÉDIA_TIPO_I, DP_TIPO_I, IC_TIPO_I,
%       MÉDIA_TIPO_II, DP_TIPO_II, IC_TIPO_II ])
machine_results = zeros(length(intervalo), 15, 2);
ind = 1; % Índice da matriz acima.
dimensao = 1; % Índice da dimensão.

% Taxas de compressão de todos os k de um algoritmo.
taxa_comp = zeros(length(intervalo), 1);
taxa_comp_idx = 1; % Índice



% Percorre todos os algoritmos
for a = 1:size(algoritmos, 2)
    alg = algoritmos{a};
    
    % Percorre todos os valores de k do algoritmo atual.
    for k = intervalo
        %% Obtenção da Taxa de Compressão para Este k.
        arq = strcat(dataHome, f, alg, f, num2str(k), f, 'Compression_Hora_12_COL_2.csv');
        M = csvread(arq);
        tc = 1 - ( M(235,2) / M(235,1) );
        taxa_comp(taxa_comp_idx, 1) = tc;
        taxa_comp_idx = taxa_comp_idx + 1;
        
        % Modo 'Original' e 'Aproximado'.
        for m = 1:size(modo, 2);
            tipo = modo{m};
            
            % Se não é para refazer a detecção no original e isso já foi
            % feito, então pulamos.
            if (strcmp(tipo, 'Orig') == 1 && repetirOrig == 0 && orig_done == 1)
                % Passa para a dimensão dos dados aproximados.
                dimensao = 2;
                continue;
            end
            
            % Status.
            fprintf('(%s) Detecção para k = %.2f. (%s)\n', alg, k, tipo);
            %% Separação Dos Dados De Treino E De Teste
            % Abre o arquivo com as features para este k.
            arq = strcat(dataHome, f, alg, f, num2str(k), f, 'Features_', tipo, '_Hora_12_COL_2.bin');
            features = abrir(arq,[234,3]);
            %features = features(:,3); % Para testar com features individuais.

            % Onde começam e terminam as instâncias positivas e negativas.
            initNegatives = 1;
            endNegatives = 197;
            initPositives = endNegatives + 1;
            endPositives = size(features,1);

            % Dados para treinamento: apenas 80% das instâncias não-danificadas.
            initTrain = initNegatives;
            endTrain = round(endNegatives * 0.8);

            learnData = features(initTrain:endTrain, :);
            nTrain = size(learnData,1); % Número de instâncias para treinamento.

            % Dados para teste: O restante dos negativos mais todos os positivos.
            initTest = endTrain + 1;
            endTest = size(features,1);

            scoreData = features(initTest:endTest, :);
            
            nUndamaged = endNegatives - endTrain;
            nTest = size(scoreData,1);

            %% Detecção de Danos
            % Vetores para a acurácia e erros do Tipo I e II.
            accuracy = zeros(numExec, 1);
            precision = zeros(numExec, 1);
            recall = zeros(numExec, 1);
            type_1 = zeros(numExec, 1);
            type_2 = zeros(numExec, 1);
            
            for run=1:numExec
                % Status.
                %fprintf_r('Execução: %d/%d', [run, numExec]);
                
                % Treinamento
                %[model]=learnMahalanobis_shm(learnData);
                [model]=learnNLPCA_shm(learnData, 5, 10, 10, 50);
                %[model]=learnFactorAnalysis_shm(learnData,1,'thomson');

                % Gera os scores para os dados de teste.
                %[DI]=scoreMahalanobis_shm(scoreData, model);
                [DI] = scoreNLPCA_shm(scoreData, model);        
                %[DI]=scoreFactorAnalysis_shm(scoreData,model);

                % Os scores são negativos mas a visualização e análise ficam
                %  melhores quando eles são positivos.
                DI = -DI;

                % Os scores para os dados de treino servem para definir um limiar.
                %[threshold]=scoreMahalanobis_shm(learnData, model);
                [threshold] = scoreNLPCA_shm(learnData, model);
                % [threshold]=scoreFactorAnalysis_shm(learnData,model);

                % Com os scores em ordem crescente, usaremos o score que está a 95%
                % do conjunto. Isso serve como uma margem de confiança.
                threshold = sort(-threshold);
                UCL = threshold(round(length(threshold)*0.95));

                % Gera o vetor de resultados: se um score passar do limiar, então
                % a instância tem dano (1), senão é saudável (0).
                results = DI >= UCL;

                % A quantidade de negativos e positivos usados na fase de teste.
                qtdeTestNegatives = endNegatives - endTrain;
                qtdeTestPositives = endPositives - endNegatives;
               
                % Calcula os dados da matriz confusão.                
                trueNegatives = sum(results(1:qtdeTestNegatives) == 0);
                falsePositives = sum(results(1:qtdeTestNegatives) == 1);
                
                truePositives = sum(results(qtdeTestNegatives+1:end) == 1);                
                falseNegatives = sum(results(qtdeTestNegatives+1:end) == 0);
                
                % Métricas de desempenho.
                % Acurácia.
                acc = (truePositives + trueNegatives)/(qtdeTestNegatives + qtdeTestPositives);
                
                % Precisão.
                if truePositives == 0 && falsePositives == 0
                    % A precisão é 1 pois não há resultados espúrios, já
                    % que todos os negativos foram descobertos.
                    pre = 1;
                else
                    pre = truePositives / (truePositives + falsePositives);
                end
                
                % Recall.
                if truePositives == 0 && falseNegatives == 0
                    % O recall é 1 pois todos os TPs (nenhum) foram
                    % descobertos.
                    rec = 1;
                else
                    rec = truePositives / (truePositives + falseNegatives);
                end
                
                % Guarda os dados desta execução.
                accuracy(run,1) = acc;
                precision(run,1) = pre;
                recall(run,1) = rec;
                type_1(run,1) = falsePositives;
                type_2(run,1) = falseNegatives;
                
                
                %------------------------------------
                % Gráfico com a curva ROC.
                %testLabels = [zeros(nUndamaged, 1); ones(nTest - nUndamaged, 1)];
                %plotROC(DI, testLabels);
                                
                % Gráfico com os indicadores de dano (DI).
                endNeg = endNegatives - initTest + 1;
                plotDI(DI, UCL, endNeg);
                return;
            end
            % Apaga a linha de status.
            %fprintf_r('',[]);
            
            % Média, desvio-padrão e IC95 para os dados.
            machine_results(ind, 1, dimensao) = mean(accuracy);
            machine_results(ind, 2, dimensao) = std(accuracy);
            machine_results(ind, 3, dimensao) = 1.96 * std(accuracy) / sqrt(numExec);
            
            machine_results(ind, 4, dimensao) = mean(precision);
            machine_results(ind, 5, dimensao) = std(precision);
            machine_results(ind, 6, dimensao) = 1.96 * std(precision) / sqrt(numExec);
            
            machine_results(ind, 7, dimensao) = mean(recall);
            machine_results(ind, 8, dimensao) = std(recall);
            machine_results(ind, 9, dimensao) = 1.96 * std(recall) / sqrt(numExec);
            
            machine_results(ind, 10, dimensao) = mean(type_1);
            machine_results(ind, 11, dimensao) = std(type_1);
            machine_results(ind, 12, dimensao) = 1.96 * std(type_1) / sqrt(numExec);
            
            machine_results(ind, 13, dimensao) = mean(type_2);
            machine_results(ind, 14, dimensao) = std(type_2);
            machine_results(ind, 15, dimensao) = 1.96 * std(type_2) / sqrt(numExec);
            
            % Se não for para repetir a detecção nos dados originais, então
            % copia as estatísticas desta execução para todos os limiares.
            if strcmp(tipo, 'Orig') && repetirOrig == 0
                
                machine_results(:,:,1) = repmat(machine_results(1,:,1), size(machine_results,1), 1);                
                % Marca como feito.
                orig_done = 1;
            end
            
            % Passa para a dimensão dos dados aproximados.
            dimensao = 2;
        end
        % Passa para o próximo k.
        ind = ind + 1;
        % Volta para a dimensão dos dados originais.
        dimensao = 1;        
    end

    %% Salva os arquivos .txt com os dados dos gráficos.
    fprintf('Escrevendo arquivos de tabelas para %s.\n', alg);
    
    % Influência da compressão (Taxa de compressão x Acurácia).
    influencia = zeros(length(intervalo), 20);
    influencia(:,1) = intervalo'; % K
    influencia(:,2) = taxa_comp; % Taxa de compressão.
    
    influencia(:,3) = machine_results(:,1,1); % Média da acurácia de f
    influencia(:,4) = machine_results(:,1,2); % Média da acurácia de f'
    influencia(:,5) = machine_results(:,2,1); % Desvio-padrão da acurácia de f
    influencia(:,6) = machine_results(:,2,2); % Desvio-padrão da acurácia de f'
    influencia(:,7) = machine_results(:,3,1); % IC95 da acurácia de f
    influencia(:,8) = machine_results(:,3,2); % IC95 da acurácia de f'
    
    influencia(:,9) = machine_results(:,4,1); % Média da precisão de f
    influencia(:,10) = machine_results(:,4,2); % Média da precisão de f'
    influencia(:,11) = machine_results(:,5,1); % Desvio-padrão da precisão de f
    influencia(:,12) = machine_results(:,5,2); % Desvio-padrão da precisão de f'
    influencia(:,13) = machine_results(:,6,1); % IC95 da precisão de f
    influencia(:,14) = machine_results(:,6,2); % IC95 da precisão de f'
    
    influencia(:,15) = machine_results(:,7,1); % Média do recall de f
    influencia(:,16) = machine_results(:,7,2); % Média do recall de f'
    influencia(:,17) = machine_results(:,8,1); % Desvio-padrão do recall de f
    influencia(:,18) = machine_results(:,8,2); % Desvio-padrão do recall de f'
    influencia(:,19) = machine_results(:,9,1); % IC95 do recall de f
    influencia(:,20) = machine_results(:,9,2); % IC95 do recall de f'

    outfile = strcat(dataOut, f, alg, '_', className, '_influencia(teste).txt');
    header=sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s',...
                    'k','Comp_ratio',...
                    'Avg_acc_f','Avg_acc_f_aprox',...
                    'Std_acc_f','Std_acc_f_aprox',...
                    'IC_acc_f','IC_acc_f_aprox',...
                    'Avg_pre_f','Avg_pre_f_aprox',...
                    'Std_pre_f','Std_pre_f_aprox',...
                    'IC_pre_f','IC_pre_f_aprox',...
                    'Avg_rec_f','Avg_rec_f_aprox',...
                    'Std_rec_f','Std_rec_f_aprox',...
                    'IC_rec_f','IC_rec_f_aprox');
    dlmwrite(outfile, header, 'delimiter', '');
    dlmwrite(outfile, influencia, 'delimiter', '\t', '-append');

    % Erros do Tipo I e do Tipo II
    qtdeErros = zeros(length(intervalo), 13);
    qtdeErros(:,1) = intervalo'; % K
    qtdeErros(:,2) = machine_results(:,10,1); % Média de erros Tipo I para f
    qtdeErros(:,3) = machine_results(:,10,2); % Média de erros Tipo I para f'
    qtdeErros(:,4) = machine_results(:,13,1); % Média de erros Tipo II para f
    qtdeErros(:,5) = machine_results(:,13,2); % Média de erros Tipo II para f'
    qtdeErros(:,6) = machine_results(:,11,1); % Desvio-padrão dos erros Tipo I para f
    qtdeErros(:,7) = machine_results(:,11,2); % Desvio-padrão dos erros Tipo I para f'
    qtdeErros(:,8) = machine_results(:,14,1); % Desvio-padrão dos erros Tipo II para f
    qtdeErros(:,9) = machine_results(:,14,2); % Desvio-padrão dos erros Tipo II para f'
    qtdeErros(:,10) = machine_results(:,12,1); % IC95 dos erros Tipo I para f
    qtdeErros(:,11) = machine_results(:,12,2); % IC95 dos erros Tipo I para f'
    qtdeErros(:,12) = machine_results(:,15,1); % IC95 dos erros Tipo II para f
    qtdeErros(:,13) = machine_results(:,15,2); % IC95 dos erros Tipo II para f'

    outfile = strcat(dataOut, f, alg, '_', className, '_erros(teste).txt');
    header=sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s','k',...
                    'Avg_T1_f','Avg_T1_f_aprox',...
                    'Avg_T2_f','Avg_T2_f_aprox',...
                    'Std_T1_f','Std_T1_f_aprox',...
                    'Std_T2_f','Std_T2_f_aprox',...
                    'IC_T1_f','IC_T1_f_aprox',...
                    'IC_T2_f','IC_T2_f_aprox');
    dlmwrite(outfile, header, 'delimiter', '');
    dlmwrite(outfile, qtdeErros, 'delimiter', '\t', '-append');
    
    % Reinicia a matriz com os dados estatísticos.
    if (repetirOrig == 0 && orig_done == 1)
        % Apaga só os dados vindos dos arquivos aproximados.
        machine_results(:,:,2) = zeros(size(machine_results,1), size(machine_results,2), 1);
    else
        % Apaga tudo.
        machine_results = zeros(size(machine_results,1), size(machine_results,2), 2);
    end
    
    % Reinicia o k.
    ind = 1;
    % Reseta a dimensão.
    dimensao = 1;
    
    % Reseta a taxa de compressão
    taxa_comp = zeros(length(intervalo), 1);
    taxa_comp_idx = 1; % Índice

end

timestamp;

return;


%% Gráficos


% Plota a curva ROC
% Os scores devem diminuir para os casos com dano.
[truePositives,falsePositives] = ROC_shm(-DI,testLabels);
figure;
plot(falsePositives, truePositives);
xlabel('falsePositives');
ylabel('truePositives');
title('ROC curve');

