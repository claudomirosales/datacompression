function [ Segment ] = stratified_sampling( SimpleData )
%STRATIFIED_SAMPLING Summary of this function goes here
%   Detailed explanation goes here

% Initialize PRNG
rng(29182);

% Count the examples
n = size(SimpleData,1);

% Locate observations in each class
ClassA = (SimpleData(:,end) == 0);
ClassB = (SimpleData(:,end) == 1);

% We already know these, but in real-life they'd need to be calculated
nClassA = sum(double(ClassA));
nClassB = sum(double(ClassB));

% Create train/test code values
Train = 1;
Test = 2;

% Allocate space for train/test indicator
Segment = repmat(Test,n,1); % Default to the last group

% Determine distinct strata
DistinctStrata = unique(SimpleData(:,end));

% Count distinct strata
nDistinctStrata = size(DistinctStrata,1);

% Loop over strata
for Stratum = 1:nDistinctStrata

    % Establish region of interest
    ROI = find(SimpleData(:,end) == DistinctStrata(Stratum));

    % Determine size of region of interest
    nROI = length(ROI);

    % Generate a scrambled ordering of 'nROI' items
    R = randperm(nROI);

    % Assign appropriate number of units to Training group
    Segment(ROI(R(1:round(0.70 * nROI)))) = Train;
end


end

