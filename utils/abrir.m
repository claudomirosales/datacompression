function data = abrir( arquivo, dim )
%ABRIR Abre um arquivo binário em uma matriz.
%   Entrada:
%       arquivo: nome do arquivo a ser lido.
%       dim: dimensão da matriz onde serão inseridos os dados.
%   Saída:
%       data: dados em formato de matriz.
%   Exemplo:
%       x = abrir('exemplo.bin', [65536,4]);

fileID = fopen(arquivo);
data = fread(fileID,dim,'double');
fclose(fileID);
end

