function plotROC( DI, labels)
%PLOTROC Plota a curva ROC para uma determinada classificação.
%
%ENTRADA
%   DI: Scores obtidos a partir da classificação. Eles devem aumentar para
%   instâncias danificadas.
%   labels: Vetor de classificação binária de todos as instâncias. 0 para
%   saudável e 1 para danificado.

[truePositives, falsePositives] = ROC_shm(-DI, labels);
figure;
plot(falsePositives, truePositives);
xlabel('False Positives');
ylabel('True Positives');
title('ROC curve');

end

