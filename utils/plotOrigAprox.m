function plotOrigAprox(orig_sinal, aprox_sinal)
%PLOTORIGAPROX Cria uma figure com o sinal original e o aproximado
%sobrepostos.
% 
%ENTRADA:
%   orig_sinal: Sinal original.
%   aprox_sinal: Sinal aproximado.

if isempty(aprox_sinal)
    error('aprox_data não pode ser vazio.');
elseif isempty(orig_sinal)
    error('orig_data não pode ser vazio.');
end

figure;
plot(orig_sinal,'k'); hold on;
plot(aprox_sinal,'b');

title('Original signal X Approximated signal');
legend('Original signal', 'Approximated signal', 'Location', 'NorthWest');
xlabel('Readings');
ylabel('Amplitude');

end