function sensorsFFT(observ, observFim, varargin)
%SENSORSFFT Plota a FFT de todos os sensores.
%   Esta função compara as FFTs de cada sensor com a FFT de todos os
%   sensores juntos. A FFTs em questão são médias das FFTs durante o
%   período especificado. Isso permite distinguir com um certo grau de
%   certeza quais frequências naturais cada sensor é capaz de captar.
%   As FFTs são extraídas das medições brutas da ponte Z-24 que foram
%   feitas às 12h.
%
%ENTRADA
%   observIni: Número da observação inicial. Valores possíveis: 1 a 234.
%   observFim: Número da observação final. Valores possíveis: 1 a 234.
%   (OPCIONAL) salvar: Se 1, salva o gráfico como imagem no diretório do
%   projeto. Se 0, não salva. Valor padrão: 0.
%   (OPCIONAL) outFile: Caminho da imagem de sáida do gráfico. Valor
%       padrão: 'results/FFTs/sensorsFFT.png'
%
%EXEMPLO
%   Calcular a PSD média da primeira semana de observações para todos os
%   sensores:
%   >> sensorsFFT(1, 7);

addpath('./utils/');

%% Verificação dos argumentos de entrada.
% No máximo 2 argumentos opcionais.
numvarargs = length(varargin);
if numvarargs > 2
    error('Máximo de 2 argumentos opcionais permitidos.');
end

% Valores default dos argumentos opcionais.
optargs = {0 'results/FFTs/sensorsFFT.png'};

% Sobrescreve os valores default com os especificados.
optargs(1:numvarargs) = varargin;

% Valores finais das variáveis opcionais.
[salvar, outFile] = optargs{:};

%% Obtenção das leituras da Z24.
% Usaremos todas as leituras feitas em uma determinada hora.
dirName = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';
hora='12'; % Hora do monitoramento. Valores possíveis 00-23.
pattern=strcat('*_',hora,'.mat'); % Padrão para buscar leituras de uma determinada hora.
files = dir( fullfile(dirName, pattern) );
files = {files.name}'; % Todos os arquivos .mat com as leituras da hora.

%% Mapa
% Mapa com as colunas da matriz (chaves) e os sensores da ponte (valores).
k = {1, 2, 3, 4, 5, 6, 7, 8};
v = {'3', '5', '6', '7', '10', '12', '14', '16'};
sensorMap = containers.Map(k, v);
tamMap = size(sensorMap, 1);

%% Leitura dos arquivos e cálculo da PSD.
% Matriz com todas as FFTs.
% (:, [FFT_SENSOR1, ..., FFT_SENSORN, FFT_TOTAL], QTDE_DIAS).
ffts_raw = zeros(32768, tamMap + 1, observFim - observ + 1);

% Dia atual.
dim = 1;

for i = observ:observFim
    % Caminho completo do arquivo .mat.
    fname = fullfile(dirName, files{i});
    % Carrega os dados como struct.
    tmp = load(fname);
    % Obtém apenas a matriz desta observação com os 8 canais.
    dataset = tmp.data;
    
    % FFT de cada sensor.
    for x=1:tamMap
        f_single = fft_anpsd(dataset(:, x));
        ffts_raw(:, x, dim) = f_single;        
    end
    
    % FFT de todos os sensores.
    f_total = fft_anpsd(dataset(:,:));
    ffts_raw(:, end, dim) = f_total;
    
    % Passa para o próximo dia (observação).
    dim = dim + 1;
end

%% Cálculo da PSD média
psd_mean = mean(ffts_raw, 3);

%% Plotagem
graf = figure;

% FFT de um único sensor
for i=1:tamMap
    subplot(4, 2, i);
    
    line1 = plot(abs(psd_mean(:,end)), 'k');    
    hold on;
    line2 = plot(abs(psd_mean(:,i)), 'r');
    
    t = strcat('Sensor #', sensorMap(i));
    title(t);
    xlabel('Frequency');
    ylabel('Amplitude');
    legend([line1, line2], {'PSD from all sensors', 'PSD from this sensor'});
end

%% Salva o gráfico, se necessário
fig = gcf;
set(fig, 'PaperUnits', 'centimeters');
set(fig, 'PaperPosition', [0 0 30 50]);
set(fig, 'PaperPositionMode', 'manual');

if salvar == 1
    saveas(graf, outFile);
    fprintf('Imagem salva em: %s\n', outFile);
    close all;
end

end