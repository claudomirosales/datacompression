% Cria uma figure com a correlação entre o sinal original e o sinal
% aproximado. Quanto mais próximo de uma reta, melhor a correlação,
% indicando que os sinais são semelhantes.
% 
% Entrada: 
%           orig_sinal: Sinal original.
%           aprox_sinal: Sinal aproximado.
function plotCorr(orig_sinal,aprox_sinal)

if isempty(aprox_sinal)
    error('aprox_data não pode ser vazio.');
elseif isempty(orig_sinal)
    error('orig_data não pode ser vazio.');
end

figure
plot(orig_sinal,aprox_sinal);

title('Correlação entre o sinal original e sinal aproximado');
xlabel('Sinal original');
ylabel('Sinal aproximado');

end