function [ comp_data ] = slideFilter( orig_data, epsilon )
%SLIDEFILTER Compressão de dados usando o Slide Filters.
%   A compressão funciona de forma similar a compressão do PWLH, no
%   entanto, o tamanho da janela é determinado pela melhor reta que pode
%   representar os dados.
%
%ENTRADA:
%   orig_data: Vetor coluna com os dados originais.
%   epsilon: Limiar de compressão.
%
%SAÍDA:
%   comp_data: Dados comprimidos. (:, [ID VALOR CONECTADO?]).
%


% Se o array de dados só possui as medições, então criamos as ID's.
if(size(orig_data,2) == 1)
   orig_data = [ (1:length(orig_data))' orig_data]; 
end

% orig_data(:,2) = normalize_var(orig_data(:,2), 0, 1);
% epsilon = std(orig_data(:,2));

% Quantidade de leituras nos dados originais
tam = size(orig_data, 1);

begin_point = 1;

% Inicializa o limite superior e inferior.
t1 = orig_data(1,1);
v1 = orig_data(1,2);
t2 = orig_data(2,1);
v2 = orig_data(2,2);

curL = zeros(1, 6);
curU = zeros(1, 6);
[curL curU] = initializeU_L(t1, v1, t2, v2, epsilon, curL, curU);

% Segmento anterior
prevG = zeros(1, 6);

% Lê os dados
upperValue = 0;
lowerValue = 0;

% Dados comprimidos
comp_data = zeros(tam, 3);
% Índice atual dos dados comprimidos
idx_comp = 1;

for i=3:tam
    itemID = orig_data(i, 1);
    itemValue = orig_data(i, 2);
    
    % Se não é o fim da entrada, lê.
    if i < tam        
        upperValue = getValue(curU, itemID);
        lowerValue = getValue(curL, itemID);
    end
        
    if (i == tam) || (itemValue - upperValue > epsilon) || (lowerValue - itemValue > epsilon)
        % Recording Mechanism
        [i curU curL begin_point prevG comp_data idx_comp] =...
        recording_mechanism(i, tam, curU, curL, orig_data, begin_point, prevG, comp_data, idx_comp, epsilon);
    else
        % Filtering Mechanism
        [curU, curL] = filtering_mechanism(i, curU, curL, orig_data, begin_point, epsilon);
    end
        
end


% Remove o que não foi usado.
comp_data = comp_data(1:idx_comp-1,:);
    
end

% Gera segmentos de linha para os intervalos de filtragem e atualiza o
% último ponto executado.
function [position curU curL begin_point prevG comp_data idx_comp] =...
    recording_mechanism(position, tam, curU, curL, orig_data, begin_point, prevG, comp_data, idx_comp, epsilon)
        
    begin_curSeg_ID = orig_data(begin_point, 1);
    
    [existInter curU curL] = updateUandLforConnectedSegment(curU, curL, prevG, begin_point, orig_data);
    
    curG = getFittestLine_G(begin_point, position, curU, curL, orig_data);
    
    if begin_point == 1
        % Cria primeiro dado a ser gravado.
        % Entry: [ID VALUE CONNTOFOLLOW].
        % Guarda
        comp_data(idx_comp,:) = [begin_curSeg_ID getValue(curG, begin_curSeg_ID) 1];
        idx_comp = idx_comp + 1;
    elseif existInter == 1
        % curG corta prevG em uma seção válida.
        inter = getIntersection(curG, prevG);
        X = 1; Y = 2;
        % Guarda
        comp_data(idx_comp,:) = [inter(X) inter(Y) existInter];
        idx_comp = idx_comp + 1;        
    else
        % curG corta prevG em uma seção inválida.
        end_prevSeg_ID = orig_data(begin_point - 1, 1);
        
        % Guarda
        comp_data(idx_comp,:) = [end_prevSeg_ID getValue(prevG, end_prevSeg_ID) existInter];
        idx_comp = idx_comp + 1;
        
        % Guarda
        comp_data(idx_comp,:) = [begin_curSeg_ID getValue(curG, begin_curSeg_ID) 1];
        idx_comp = idx_comp + 1;
    end
    
    
    if position < tam
        % Cria novo intervalo por dois pontos
        begin_point = position;
        curItem_ID = orig_data(position, 1);
        curItem_Value = orig_data(position, 2);
        position = position + 1;
        nextItem_ID = orig_data(position, 1);
        nextItem_Value = orig_data(position, 2);
        
        [curL curU] =  initializeU_L(curItem_ID, curItem_Value, nextItem_ID, nextItem_Value, epsilon, curL, curU);
        prevG = curG;
    elseif position == tam
        % Se último intervalo possui apenas um ponto, cria o último dado e
        % termina a compressão.
        begin_point = position;
        preItem_ID = orig_data(position - 1, 1);
        
        item_ID = orig_data(position, 1);
        item_Value = orig_data(position, 2);
        
        % Guarda
        comp_data(idx_comp,:) = [preItem_ID getValue(curG, preItem_ID) 1];
        idx_comp = idx_comp + 1;
        
        % Guarda
        comp_data(idx_comp,:) = [item_ID item_Value 0];
        idx_comp = idx_comp + 1;
        
        position = position + 1;
    end
%     else
%         item_ID = orig_data(position - 1, 1);
%         
%         Guarda
%         comp_data(idx_comp,:) = [item_ID getValue(curG, item_ID) 0];
%         idx_comp = idx_comp + 1;        
%     end
end

% Ajusta o limite superior e inferior com a chegada de cada novo ponto.
function [curU, curL] = filtering_mechanism(position, curU, curL, orig_data, begin_point, epsilon)
    SLOPE = 5; INTERCEPT = 6;
    
    item_ID = orig_data(position, 1);
    item_Value = orig_data(position, 2);
    
    upperValue = getValue(curU, item_ID);
    lowerValue = getValue(curL, item_ID);
    
    top = [item_ID (item_Value + epsilon)];
    bottom = [item_ID (item_Value - epsilon)];
    
    % Item está acima de L, uma distância que é maior que epsilon.
    if item_Value - lowerValue > epsilon
        % Atualiza L
        for j=begin_point:position-1
            tmpItem_ID = orig_data(j, 1);
            tmpItem_Value = orig_data(j, 2);
            
            tmpTop = [tmpItem_ID (tmpItem_Value + epsilon)];
            tmpL = [tmpTop bottom calc_slope(tmpTop, bottom)];
            
            tmpL_slope = tmpL(SLOPE);
            curL_slope = curL(SLOPE);
            
            if tmpL_slope > curL_slope
                curL(SLOPE) = tmpL_slope;
                curL(INTERCEPT) = tmpL(INTERCEPT);
            end
            
        end
    end
    
    % Item está abaixo de U, uma distância que é maior que epsilon.
    if upperValue - item_Value > epsilon
        % Atualiza U
        for j=begin_point:position-1
            tmpItem_ID = orig_data(j, 1);
            tmpItem_Value = orig_data(j, 2);
            
            tmpBottom = [tmpItem_ID (tmpItem_Value - epsilon)];
            tmpU = [tmpBottom top calc_slope(tmpBottom, top)];
            
            tmpU_slope = tmpU(SLOPE);
            curU_slope = curU(SLOPE);
            
            if tmpU_slope < curU_slope
                curU(SLOPE) = tmpU_slope;
                curU(INTERCEPT) = tmpU(INTERCEPT);
            end            
        end
    end
end

function [existInter curU curL] = updateUandLforConnectedSegment(curU, curL, prevG, begin_point, orig_data)
    if begin_point == 1
        existInter = 0;
        return;
    end
    
    SLOPE = 5;
    
    ul = getIntersection(curU, curL);
    
    preItem_ID = orig_data(begin_point - 1, 1);
    
    firstItem_ID = orig_data(begin_point, 1);
    
    % Calcula os pontos alpha e beta.
    alpha = [preItem_ID getValue(prevG, preItem_ID)];
    beta = [firstItem_ID getValue(prevG, firstItem_ID)];
    
    alpha_ul = [ul alpha calc_slope(ul, alpha)];
    beta_ul = [ul beta calc_slope(ul, beta)];
        
    if alpha_ul(SLOPE) < beta_ul(SLOPE)
        minSlope = alpha_ul(SLOPE);
        maxSlope = beta_ul(SLOPE);
    else
        maxSlope = alpha_ul(SLOPE);
        minSlope = beta_ul(SLOPE);
    end
    
    % Verifica se segmento atual está conectado com o anterior.
    isConnected = 0;
    
    if minSlope < curU(SLOPE) && maxSlope > curL(SLOPE)
        %ul = getIntersection(curU, curL);
        isConnected = 1;
        
        % Atualiza os limites
        if maxSlope < curU(SLOPE)
            curU = updateLine(curU, ul, maxSlope);
        end
        
        if minSlope > curL(SLOPE)
            curL = updateLine(curL, ul, minSlope);
        end
        
    end
    
    existInter = isConnected;
    
end

% Encontra a melhor linha dentro da região entre o limite inferior e
% superior.
function [fittestLine] = getFittestLine_G(beginPoint, endPoint, curU, curL, orig_data)
    X = 1; Y = 2; SLOPE = 5; INTERCEPT = 6;
    
    ul = getIntersection(curU, curL);
    
    numberator = 0;
    denominator = 0;
    
    for j=beginPoint:endPoint
        item_ID = orig_data(j, 1);
        item_Value = orig_data(j, 2);
        
        numberator = numberator + ( (item_Value - ul(Y)) * (item_ID - ul(X)) );
        denominator = denominator + ( (item_ID - ul(X)) * (item_ID - ul(X)) );        
    end
    
    if denominator == 0
        keyboard;
    end
    
    fraction = numberator / denominator;
    slopeU = curU(SLOPE);
    slopeL = curL(SLOPE);
    
    % Obtém o melhor slope dentro da área do limite inferior e superior.
    if fraction > slopeU
        fittestSlope = slopeU;
    else
        fittestSlope = fraction;
    end
    
    if fittestSlope <= slopeL
        fittestSlope = slopeL;
    end
    
    % Cria a melhor linha.
    fittestLine = zeros(1, 6);
    fittestLine(SLOPE) = fittestSlope;
    fittestLine(INTERCEPT) = ul(Y) - (fittestSlope * ul(X));
end

function [curL curU] =  initializeU_L(t1, v1, t2, v2, epsilon, curL, curU)
    top1 = [ t1 (v1 + epsilon) ];
    bottom1 = [ t1 (v1 - epsilon) ];
    top2 = [ t2 (v2 + epsilon) ];
    bottom2 = [ t2 (v2 - epsilon) ];
    
    curL = updateLinePoint(curL, top1, bottom2);
    curU = updateLinePoint(curU, bottom1, top2);
end

%% Line Functions
function [slope_intercept] = calc_slope(point1, point2)
    X = 1; Y = 2;
    
    if point1(X) ~= point2(X)
        slope = (point2(Y) - point1(Y)) / (point2(X) - point1(X));
        intercept = point1(Y) - slope * point1(X);    
        slope_intercept = [slope intercept];        
    else
        slope_intercept = [0 0];
    end
    
end

function [value] = getValue(line, timestamp)
    SLOPE = 5; INTERCEPT = 6;
    value = line(SLOPE) * timestamp + line(INTERCEPT);
end

function [interPoint] = getIntersection(line1, line2)
    X = 1; Y = 2; SLOPE = 5; INTERCEPT = 6;
    
    interPoint = [0 line1(INTERCEPT)];
    
    d = line1(SLOPE) - line2(SLOPE);
    
    if d ~= 0
        interPoint(X) = int32( (line2(INTERCEPT) - line1(INTERCEPT)) / d );
        interPoint(Y) =  line1(SLOPE) * interPoint(X) + line1(INTERCEPT) ;
    end
    
end

function [line] = updateLine(line, point, slope)
    X = 1; Y = 2; SLOPE = 5; INTERCEPT = 6;
    
    line(SLOPE) = slope;
    line(INTERCEPT) = point(Y) - line(SLOPE) * point(X);
    
end

function [line] = updateLinePoint(line, point1, point2)
    X = 1; Y = 2; SLOPE = 5; INTERCEPT = 6;
    
    if point1(X) ~= point2(X)
        line(SLOPE) = (point2(Y) - point1(Y)) / (point2(X) - point1(X));
        line(INTERCEPT) = point1(Y) - line(SLOPE) * point1(X);                
    end
end