function certaintyGraphs( alg )
%CERTAINTYGRAPHS Cria os gráficos de erros e acurácia.
%   Este script cria os gráficos de erros do tipo I/II e da acurácia
%   baseando-se nos dados estatísticos da detecção de danos. Assim, para
%   cada algoritmo de compressão, são feitos dois gráficos: o de erros e o
%   de acurácia. O eixo x corresponde aos limiares de compressão. A
%   detecção foi executada várias vezes em cada limiar, por isso, também é
%   plotado uma margem de erro com 95% de confiança.
%
%ENTRADA:
%   alg: Conjunto com os nomes dos algoritmos desejados. Ex.:
%       alg = {'cheb', 'pca', 'apca', 'pwlh', 'sf'}.

%% Variáveis auxiliares.
% Separador de arquivos da plataforma.
f = filesep;

% Quantidade de algoritmos a serem analisados.
qtdeAlg = size(alg, 2);

% Diretório onde estão os arquivos com dados brutos.
inputDir = 'results';

% Diretório de saída das imagens contendo os gráficos.
%outDir = 'graphs/damage_detection';
outDir = 'results';

for a = 1:qtdeAlg
    %% Arquivo contendo os erros do tipo I e II.
    dadosErros = strcat(inputDir,f, alg{a}, '_nlpca_erros.txt');
    M = tdfread(dadosErros);

    graf = figure;
    A = shadedErrorBar(M.k, M.Avg_T1_f, [M.IC_T1_f], {'-b','LineWidth',1.5});
    hold on;
    B = shadedErrorBar(M.k, M.Avg_T1_f_aprox, [M.IC_T1_f_aprox], {'--b','LineWidth',1.5},1);
    C = shadedErrorBar(M.k, M.Avg_T2_f, [M.IC_T2_f], {'-r','LineWidth',1.5},1);
    D = shadedErrorBar(M.k, M.Avg_T2_f_aprox, [M.IC_T2_f_aprox], {'--r','LineWidth',1.5},1);

    t = strcat('Errors produced by NLPCA when using approximated data from',{' '}, upper(alg{a}));
    title(t);
    xlabel('Constant{\it k} used in threshold \epsilon = k \cdot \sigma (f)');
    ylabel('Occurrences');
    legend([A.mainLine, B.mainLine, C.mainLine, D.mainLine], 'Average occurrences of Type I in f',...
    'Average occurrences of Type I in f''',...
    'Average occurrences of Type II in f',...
    'Average occurrences of Type II in f''', 'Location','northwest');

    % Salva o gráfico como uma imagem PNG.
    saida = strcat(outDir, f, upper(alg{a}), '-errors.png');
    saveas(graf, saida);

    %% Arquivo contendo a acurácia/precisão/recall da detecção.
    dadosInfl = strcat(inputDir,f, alg{a}, '_nlpca_influencia.txt');
    I = tdfread(dadosInfl);

    graf2 = figure;
    A = plot(I.k, I.Comp_ratio,'-k','LineWidth',1.5);
    hold on;
    B = shadedErrorBar(I.k, I.Avg_acc_f, [I.IC_acc_f], {'-k','LineWidth',1.5});
    C = shadedErrorBar(I.k, I.Avg_acc_f_aprox, [I.IC_acc_f_aprox], {'--k','LineWidth',1.5},1);
    D = shadedErrorBar(I.k, I.Avg_pre_f, [I.IC_pre_f], {'-g','LineWidth',1.5});
    E = shadedErrorBar(I.k, I.Avg_pre_f_aprox, [I.IC_pre_f_aprox], {'--g','LineWidth',1.5},1);
    F = shadedErrorBar(I.k, I.Avg_rec_f, [I.IC_rec_f], {'-b','LineWidth',1.5});
    G = shadedErrorBar(I.k, I.Avg_rec_f_aprox, [I.IC_rec_f_aprox], {'--b','LineWidth',1.5},1);

    t = strcat('Influence of',{' '},upper(alg{a}),{' '}, 'compression on NLPCA''s accuracy');
    title(t);
    xlabel('Constant{\it k} used in threshold \epsilon = k \cdot \sigma (f)');
    ylabel('Amplitude');
    legend([A, B.mainLine, C.mainLine, D.mainLine, E.mainLine, F.mainLine, G.mainLine],...
            'Compression ratio',...
            'Average accuracy of f',...
            'Average accuracy of f''',...
            'Average precision of f',...
            'Average precision of f''',...
            'Average recall of f',...
            'Average recall of f''',...
            'Location','southeast');

    saida = strcat(outDir, f, upper(alg{a}), '-influence.png');
    saveas(graf2, saida);

    close all;
end

end

