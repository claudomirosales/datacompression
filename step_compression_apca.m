function [comp_data, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_apca( sinal_original, threshold )
%STEP_COMPRESSION Realiza a compressão do sinal original.
%   Comprime o sinal original utilizando um algoritmo de compressão.
%   Para calcular a razão de compressão, basta fazer tam_com / tam_ori.
%   Uma razão de compressão de 0.7 (70%) significa que o sinal comprimido
%   ocupa 70% do tamanho do sinal original, isto é, houve uma redução de
%   30% no tamanho. A taxa de compressão é 1 - Razao_comp.
%   Obs.: O tamanho dos arquivos é referente às suas suas versões em
%   binário, e não ao tamanho do arquivo .mat.
%   INPUT
%       sinal_original: Matriz com as leituras de um sensor. [:, LEITURAS]
%       threshold: Limiar de compressão a ser passado para o algoritmo de
%       compressão.
%   OUTPUT
%       sinal_com: Sinal comprimido.
%       sinal_aprox: Sinal aproximado.
%       tam_ori: Tamanho do sinal original, em bytes.
%       tam_comp: Tamanho do sinal comprimido, em bytes.
%       time_comp: Tempo gasto na compressão.

%% Compressão
start = tic;
[comp_data] = apca(sinal_original, threshold);
time_comp = toc(start);


%% Descompressão (Gera o sinal aproximado).
[sinal_aprox] = pca_approx(comp_data, size(sinal_original,1));


%% Salva o sinal comprimido e o sinal aproximado em binário.
% Tamanho do arquivo original.
tam_ori = salvar('sinal_original.bin', sinal_original);

% Tamanho do arquivo comprimido
t_com = salvar('sinal_comprimido.bin', comp_data);
tam_comp = t_com;

end

