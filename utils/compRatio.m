% Compression Ratio
% 
% Moisés Felipe - Bacharelando em Ciência da Computação
% 
% Calcula a taxa de compressão.
% 
% Entrada: 
%           OrigData: Dados originais.
%           CompData: Dados comprimidos.
% Saída:
%           Ratio: Valor entre [0,1]. Um valor de 0.8 indica que os dados
%           comprimidos ocupam 80% do tamanho original.
%
%           Ex:
%               Ratio = compRatio(OrigData,CompData)
% 
function ratio = compRatio(orig_data,comp_data)

if isempty(comp_data)
    error('comp_data não pode ser vazio.');
elseif isempty(orig_data)
    error('orig_data não pode ser vazio.');
end

orig_data = length(orig_data); 

comp_data = length(comp_data);

ratio = (comp_data/orig_data);

end