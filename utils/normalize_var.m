% Normaliza os valores de um array no intervalo [x,y].
% 
% Entrada: 
%           array: Valores originais.
%           [x,y]: Intervalo para a normalização (ex.: [0,1]).
% Saída:
%           normalized: Valores normalizados.
function normalized = normalize_var(array, x, y)

     %Normaliza para [0, 1]:
     m = min(array);
     range = max(array) - m;
     array = (array - m) / range;

     %Escala para [x,y]:
     range2 = y - x;
     normalized = (array*range2) + x;
end