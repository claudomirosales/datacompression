% SALVAR Salva dados do matlab em um arquivo binário.
% Os números são gravados com precisão double.
% ENTRADA:
%   nome_arq: nome do arquivo binário (ex.: 'matriz.bin');
%   dados: array a ser salvo.
% SAÍDA:
%   tam: tamanho do arquivo binário que foi salvo.
function tam = salvar(nome_arq, dados)

fileID = fopen(nome_arq,'w');
fwrite(fileID,dados,'double');
fclose(fileID);

fileInfo = dir(nome_arq);
tam = fileInfo.bytes;

end