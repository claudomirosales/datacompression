function [comp_data, comp_control, comp_lengths] = chebyshev(orig_data, epsilon)
%CHEBYSHEV Compressão de dados utilizando polinômios de Chebyshev.
%   Este algoritmo divide o sinal em janelas de tamanho fixo (16). Para
%   cada janela, aproxima os dados pela transformação de Chebychev e
%   verifica se cada um está acima do limiar de compressão. Coeficientes
%   abaixo do limiar são aproximados para 0. Em seguida, uma control-word é
%   criada por janela, funcionando como uma espécie de máscara: 1 indica a
%   presença de um coeficiente retido, 0 indica coeficiente igual a 0. Por
%   fim, os coeficientes retidos, as control-words e os seus comprimentos
%   são retornados.
%   
%ENTRADA:
%	orig_data: Vetor coluna com os dados originais.
%	epsilon: Limiar de compressão.
%
%SAÍDA:
%	comp_data: Coeficientes retidos.
%	comp_control: Control-words.
%	comp_lengths: Tamanho das control-words.
%


%% Configuração de variáveis.
% Tamanho da janela.
window = 16;

% Quantidade de leituras.
tam = size(orig_data, 1);

% Matriz com os dados comprimidos. Ele tem o mesmo tamanho da matriz
% original para que aumente o desempenho (segundo o Matlab). No final, nós
% cortamos o que não é necessário. V = (DADOS)
comp_data = zeros(tam, 1);
% Posição atual.
comp_data_index = 1;

% Matriz com todas as control-words dos coeficientes.
comp_control = zeros(tam, 1);
% Posição atual.
comp_control_index = 1;

% Matriz com os comprimentos originais de todas as control-words.
comp_lengths = zeros(tam, 1);
% Posição atual.
comp_lengths_index = 1;
% Indica a última posição inserida.
fim_word = 1;

% Representa o início da janela
start_window = 1;

%% Compressão de dados.
while  start_window <= tam
    
    %% Determina o início e o fim da janela
    % Última posição da janela de dados.
    end_window = start_window + window - 1;
    
    % Se o final da janela passa o final dos dados, então a janela deve ir
    % do índice atual até o final dos dados.
    if end_window > tam
        end_window = tam;
    end
    
    % Por conveniência, separamos todos os dados desta janela em um vetor.
    dados = orig_data(start_window:end_window);
    % A compressão requer uma matriz quadrada.
    dados = reshape(dados, [4,4]);
    % Tamanho dos dados originais desta janela.
    N = size(dados, 1);
    
    %% Transformação de Chebyshev
    % Coeficientes de Chebyshev para todos os dados desta janela.
    coef = zeros(size(dados));

    % Somatório da transformação de Chebyshev.
    somatorio_x = 0;
    
    % Percorre todos os elementos da matriz quadrada original: X_(i,j).
    for i=1:N
        for j=1:N
            % Somatório X
            for k=1:N
                % Somatório Y
                somatorio_y = 0;
                for m=1:N
                    somatorio_y = somatorio_y + (dados(m,k) * cheb_T(i,m,N) * cheb_T(j,k,N) );
                end
                somatorio_x = somatorio_x + somatorio_y;
            end
            
            % Por fim, calcula o coeficiente para o dado atual.
            coef(i,j) = (4/(N^2)) * somatorio_x;

            % Reseta o somatório para o próximo dado.
            somatorio_x = 0;
        end
    end
    
    
    %% Thresholding.
    coef_thres = zeros(size(coef));

    for i=1:N
        for j=1:N
            if abs(coef(i,j)) >= epsilon
                coef_thres(i,j) = coef(i,j);
            else
                coef_thres(i,j) = 0;
            end
        end
    end
    
    % O primeiro elemento sempre fica.
    coef_thres(1,1) = coef(1,1);
                
    %% Control-word
    % Os coeficientes retidos são mapeados para 1 e o resto para 0.
    cw = coef_thres ~= 0;

    %% Zigzag scan
    %Transforma as matrizes de coeficientes e control-word para vetores.
    vetor_coef = zigzag(coef_thres);
    vetor_cw = zigzag(cw);

    %% Codificação dos coeficientes
    % Retira todos os zeros.
    vetor_coef( vetor_coef == 0 ) = [];
    
    %% Codificação da control-word.    
    % Procura o índice do último 1.
    last_one = length(vetor_cw);
    while last_one >= 1 && vetor_cw(last_one) ~= 1
        last_one = last_one - 1;
    end

    if last_one > 1
        % Trunca a control-word depois do último 1.
        vetor_cw = vetor_cw(1:last_one);
        % Remove o primeiro e o último 1 do vetor truncado.
        vetor_cw([1 last_one]) = [];
        % Guarda o tamanho do control-word.
        comp_lengths(comp_lengths_index, 1) = length(vetor_cw);
    else
        % Se a control-word for [1 0 0 0 ...] então 0 bits serão enviados.
        vetor_cw = [];
        % Sinalizamos esses casos com -1.
        comp_lengths(comp_lengths_index, 1) = -1;
    end
        
    % Próxima posição do array de comprimentos.
    comp_lengths_index = comp_lengths_index + 1;
    
    %% Armazenamento dos coeficientes e control-word
    % Guarda os coeficientes truncados no vetor de dados comprimidos.
    fim_cd = comp_data_index + length(vetor_coef) - 1;
    comp_data(comp_data_index:fim_cd, 1) = vetor_coef';
    comp_data_index = fim_cd + 1;
    
    % Guarda as control-words.    
    size_cw = length(vetor_cw);
    % Se a control-word é vazia, não armazenamos nada, mas saberemos da sua
    % existência pelo seu comprimento (0) que é armazenado.
    if size_cw > 0
        fim_word = comp_control_index + size_cw - 1;
        comp_control(comp_control_index:fim_word, 1) = vetor_cw';
        comp_control_index = fim_word + 1;
    end
    
    % Avança a janela.
    start_window = end_window + 1;
        
end

% Só vamos usar até onde inserimos coeficientes. O resto é descartado.
comp_data = comp_data(1:fim_cd, :);

% Retira a parte complexa.
comp_data = real(comp_data);

% Só vamos usar até onde inserimos control-words.
comp_control = comp_control(1:fim_word, :);

% Só vamos usar até onde inserimos lengths.
comp_lengths = comp_lengths(1:comp_lengths_index-1, :);

end