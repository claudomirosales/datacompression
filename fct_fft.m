function [freq,yfft] = fct_fft(x,dt)

%UNTITLED1 Summary of this function goes here
%   dt - time step
%   dataset - Time histories (columns are channels and rows are observations)

[nrow ncol]=size(x); % matrix dimensions
t = (0:nrow-1)*dt; % Time vector
Fs = 1/dt; % Sampling frequency

% Exponential window for accelerometers
hann_window=hann(nrow);

for i=1:ncol;
    m(:,i)=x(:,i).*hann_window;
end

NFFT = 2^nextpow2(nrow); % Next power of 2 from length of y(t)
freq(:,1) = Fs/2*linspace(0,1,NFFT/2); % Nyquist frequency

AUX = fft(m,NFFT)/nrow;
yfft=AUX(1:NFFT/2,:);
