function [comp_data] = pca(orig_data, epsilon)
%PCA Compressão de dados utilizando Piecewise Constant Approximation.
%   Realiza a compressão de dados usado o PCA (também conhecido como PAA -
%   Piecewise Aggregate Approximation). O algoritmo divide os dados em
%   janelas de tamanhos iguais e para cada uma é calculado o valor máximo e
%   mínimo. Além disso, é necessário uma tolerância de erro que define quando
%   os dados serão aproximados ou conservados. Isso é expresso por Max - Min
%   < 2e. O tamanho da janela utilizado é 16.
%
%ENTRADA:
%   orig_data: Vetor coluna com os dados originais.
%   epsilon: Limiar de compressão.
%
%SAÍDA:
%	comp_data: Dados comprimidos. (:, [ID VALOR]).
%


%% Configurações de variáveis.
% Tamanho da janela.
window = 16;

% No PCA, os dados são comprimidos quando Max - Min <= 2e.
epsilon = epsilon * 2;

% Se o array de dados só possui as medições, então criamos as ID's.
if(size(orig_data,2) == 1)
   orig_data = [ (1:length(orig_data))' orig_data]; 
end

% Matrix com os dados comprimidos. Ele tem o mesmo tamanho da matriz
% original para que aumente o desempenho (segundo o Matlab). No final, nós
% cortamos o que não é necessário. (ID, DADOS)
comp_data = ones(size(orig_data));

% Próxima posição vaga do array de dados comprimidos.
comp_index = 1;

% Representa o início da janela
index = 1;

% Quantidade de leituras
tam = size(orig_data, 1);

%% Algoritmo
while index <= tam
   
    % Última posição da janela de dados.
    end_bucket = index + window - 1;
    
    % Se o final da janela passa o final dos dados, então a janela deve ir
    % do índice atual até o final dos dados.
    if end_bucket > tam
        end_bucket = tam;
    end
    
    % Identifica o valor máximo e mínimo da janela.
    Max = max(orig_data(index:end_bucket, 2));
    Min = min(orig_data(index:end_bucket, 2));

    % Calcula a diferença.
    dif = Max - Min;
        
    if dif <= epsilon
        % Se a diferença for menor ou igual o erro máximo, então comprime.
        % A compressão consiste em guardar a ID e a aproximação dos dados,
        % que consiste em dif/2 + Min.
        aproximated = dif / 2 + Min;
        comp_data(comp_index,:) = [end_bucket aproximated];
        % Próxima posição vazia.
        comp_index = comp_index + 1;
    else
        % Caso contrário, preserva todos os ID's e dados.
        time_stamp = (index : end_bucket)';
        % Até onde vamos preencher o vetor de dados comprimidos.
        fim = comp_index + end_bucket - index;
        comp_data(comp_index:fim,:) = [time_stamp orig_data(index:end_bucket, 2)];
        % Próxima posição vazia.
        comp_index = fim + 1;
    end
    
    % Avança a janela.
    index = end_bucket + 1;
        
end

% Só vamos usar até onde inserimos dados comprimidos. O resto é descartado.
comp_data = comp_data(1:comp_index - 1, :);

end