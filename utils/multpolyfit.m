function [c2] = multpolyfit(x,y,n)

m = size(x,2);

c2 = zeros(n+1,m);

for k = 1:m
    M = repmat(x(:,k),1,n+1);
    M = bsxfun(@power,M,0:n);
    c2(:,k) = M\y(:,k);
end

c2 = fliplr(c2');

end