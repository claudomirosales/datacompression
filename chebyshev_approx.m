function aprox_data = chebyshev_approx(compress_data, comp_control, comp_lengths, tam_orig)
%CHEBAPROX Geração do sinal aproximado usando o Chebyshev.
%   Realiza a aproximação do sinal comprimido por polinômios de Chebyshev.
%
%ENTRADA:
%	compress_data: Coeficientes de Chebyshev.
%	comp_control: Control-words.
%	comp_lengths: Tamanho das control-words.
%	tam_orig: Tamanho do sinal original.
%
%SAÍDA:
%	aprox_data: Dados aproximados. (:, [VALOR]).
%


% Tamanho da janela.
window = 16;

% Tamanho do sinal comprimido.
tam = length(compress_data);

% Sinal aproximado deve ter o mesmo tamanho que o sinal original.
aprox_data = zeros(tam_orig, 1);

% Representa o início da janela
start_window = 1;

% Posição atual do vetor de coeficientes truncado (compress_data).
ini = 1;

% Posição atual do vetor de comprimentos.
idx_l = 1;

% Posição atual do vetor de control-words.
idx_c = 1;


%% Percorre todo o sinal comprimido.
while ini <= tam
    % Se parar na última posição dos dados comprimidos, apenas inclui no
    % sinal aproximado e encerra.
%     if ini == tam
%         aprox_data(start_window) = compress_data(ini);
%         break;
%     end
    
    %% Define a posição inicial e final da janela.
    % Última posição da janela de dados para inserir dados aproximados.
    end_window = start_window + window - 1;
    
    % Se o final da janela passa o final dos dados, então a janela deve ir
    % do índice atual até o final dos dados.
    if end_window > tam_orig
        end_window = tam_orig;
    end
    
    %% Reconstrói a control-word
    % Comprimento desta control-word.    
    len = comp_lengths(idx_l);
    
    if len == -1
        % Se o comprimento é -1, então sabemos que só um coeficiente foi
        % retido. Logo completamos o resto da janela com 0.
        cw = [1; linspace(0, 0, window - 1)'];
    elseif len == 0
        % Se for 0, foram os dois primeiros coeficientes retidos.
        cw = [1; 1; linspace(0, 0, window - 2)'];
    else
        % Até onde vamos buscar a control-word.
        fim = idx_c + len - 1;
        % Obtém os bits do control-word comprimidos.
        cw = comp_control(idx_c:fim);
        % Insere 1 na frente e atrás.
        cw(2:end+1) = cw;
        cw([1 end+1]) = 1;
        % Se ainda falta espaço para completar a janela, preenche com 0.
        if length(cw) < window            
            cw(end + 1 : window) = 0;
        end
        % Próxima posição disponível do vetor de control-words.
        idx_c = fim + 1;
    end
            
    % Próxima posição disponível do vetor de comprimentos.
    idx_l = idx_l + 1;
    
            
    %% Reconstrói o vetor de coeficientes
    % Vetor dos coeficientes originais
    dados = zeros(1, window);
    
    % Quantos 1's (coeficientes) temos que pegar.
    qtde_one = sum(cw==1);
    
    %fim do buffer dos dados comprimidos.
    final = ini + qtde_one - 1;
    
    % Trecho de interesse.
    tmp = compress_data(ini:final);
    ini = final + 1;
    
    % Onde for 1, insere o coeficiente, caso contrário, 0.
    % O tamanho da control-word sempre é igual ao tamanho da janela.
    indx = 1;
    for k=1:window
        if cw(k) == 1
            dados(k) = tmp(indx);
            indx = indx + 1;
        else
            dados(k) = 0;
        end
    end
    
    % A aproximação requer uma matriz quadrada.
    dados = izigzag(dados', 4, 4);
    
    %% Aproximação
    % Matriz para os dados aproximados desta janela.
    dados_aprox = zeros(size(dados));
    % Quantidade de dados aproximados desta janela.
    N = size(dados_aprox, 1);
    
    somatorio_a = 0;
    somatorio_b = 0;
    somatorio_c = 0;
    
    % Percorre todos os elementos da matriz quadrada comprimida: X_(i,j).
    for i = 1:N
        for j = 1:N

            %somatório A
            for k=1:N
                somatorio_a = somatorio_a + (dados(k,1) * cheb_T(k,j,N));
            end
            %somatório B
            for m=1:N
                somatorio_b = somatorio_b + (dados(1,m) * cheb_T(m,i,N));
            end
            %somatório C
            for m=1:N
                %somatório D
                somatorio_d = 0;
                for k=1:N
                    somatorio_d = somatorio_d + (dados(k,m) * cheb_T(k,j,N) * cheb_T(m,i,N));
                end
                somatorio_c = somatorio_c + somatorio_d;
            end

            dados_aprox(i,j) = 0.25 * dados(1,1) - 0.5 * (somatorio_a + somatorio_b) + somatorio_c;
    
            % Reseta os somatórios para o próximo elemento.
            somatorio_a = 0;
            somatorio_b = 0;
            somatorio_c = 0;

        end
    end
    
    %% Armazenamento
    % Redimensiona a matriz para vetor com os dados aproximados.
    dados_aprox = reshape(dados_aprox, [window, 1]);
    
    % Guarda os dados aproximados no vetor de dados aproximados.
    aprox_data(start_window:end_window) = dados_aprox;
    
    % Avança a janela.
    start_window = end_window + 1;

end

end
