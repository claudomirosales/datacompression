function plotDI( DI, UCL, endNeg )
%PLOTDI Plota o gráfico de DI para uma detecção de danos.
%   ENTRADA:
%       endNeg: Fim dos negativos no grupo de teste.

figure;
bar(1:endNeg, DI(1:endNeg), 'k'); hold on;

bar(endNeg+1:length(DI), DI(endNeg+1:end), 'r');

line('XData',[0 length(DI)+1],'YData',[UCL UCL],'Color','b','LineWidth',1,'LineStyle','-.');

title('Damage Indicators');
xlabel('State Condition');
ylabel('DI''s Amplitude');
legend('Undamaged','Damaged','Location', 'Northwest');

end

