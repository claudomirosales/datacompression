function [ comp_data ] = apca( orig_data, epsilon )
%APCA Compressão de dados usando o Adaptive PCA.
%   O APCA funciona, em essência, como o PCA, com a diferença que a janela
%   de dados possui tamanho variável. A janela cresce até que os dados
%   quebrem o limiar de compressão. Os dados, então, são aproximados por
%   Dif/2 + Min. Dados que são maiores que o limiar são preservados. O
%   processo de descompressão é o mesmo do PCA.
%
%ENTRADA:
%   orig_data: Vetor coluna com os dados originais.
%   epsilon: Limiar de compressão.
%
%SAÍDA:
%   comp_data: Dados comprimidos. (:, [ID VALOR]).
%


%% Configurando as variáveis necessárias.

% Cria as ID's.
if(size(orig_data, 2) == 1)
   orig_data = [ (1:length(orig_data))' orig_data]; 
end

% Os dados são comprimidos quando Max - Min <= 2e.
epsilon = epsilon * 2;

% Início da janela de compressão.
start_window = 1;

% Matrix com os dados comprimidos. Ele tem o mesmo tamanho da matriz
% original para que aumente o desempenho (segundo o Matlab). No final, nós
% cortamos o que não é necessário.
comp_data = ones(size(orig_data));
% Índice com a linha atual da matriz dos dados comprimidos.
idx_comp = 1;

% Tamanho dos dados originais.
tam = length(orig_data);

%% Algoritmo        
while start_window <= tam
    
    % Estamos na última posição e, logo, não há como haver mais janelas.
    % Então salvamos o último ponto e encerramos a compressão.
    if start_window == tam
        comp_data(idx_comp,:) = [orig_data(start_window,1) orig_data(start_window,2)];
        break;
    end
    
    % Tamanho inicial da janela.
    window = 1;
    
    % Fim da janela de compressão.
    end_window = start_window + window;
    
    % Diferença inicial.
    Max = max(orig_data(start_window:end_window, 2));
    Min = min(orig_data(start_window:end_window, 2));
    dif = Max - Min;
    
    % Variáveis auxiliares.
    Min_ahead = Min;
    dif_ahead = dif;
    window_ahead = window;
    end_window_ahead = end_window;

    % Enquanto a diferença for menor ou igual que o threshold, então
    % aumenta o tamanho da janela.
    while (dif_ahead < epsilon) && (end_window_ahead < tam - 1)
        % Aceita os valores da janela anterior pois não passaram o limiar.
        Min = Min_ahead;
        dif = dif_ahead;
        window = window_ahead;
        end_window = end_window_ahead;
        
        % Calcula a diferença da janela atual.
        Max_ahead = max(orig_data(start_window:end_window, 2));
        Min_ahead = min(orig_data(start_window:end_window, 2));
        dif_ahead = Max_ahead - Min_ahead;
        
        % Aumenta a janela.
        window_ahead = window_ahead + 1;

        % Atualiza o limite final da janela.
        end_window_ahead = start_window + window_ahead;        
    end
    
    % Retira o excedente do limite final, caso tenhamos passado.
    if end_window > tam
        end_window = tam;
    end
    
    % Flag indicando se houve (1) ou não (0) compressão.
    comprimiu = 0;
    
    %% Compressão    
    % Agora que temos uma janela cujos valores estão abaixo do threshold,
    % vamos representá-los por uma constante.
    if window > 1
        % Representa a janela por [ID; Dif/2 + Min].
        comp_data(idx_comp, :) = [orig_data(start_window, 1) (dif/2 + Min)];
        % Próxima linha vazia.
        idx_comp = idx_comp + 1;

        comprimiu = 1;
    else
        % Janela de tamanho 1. Se isso ocorreu é porque passamos o threshold
        % e temos que preservar os dados. Salva apenas a primeira amostra.
        comp_data(idx_comp, :) = [orig_data(start_window, 1) orig_data(start_window, 2)];
        % Próxima linha vazia.
        idx_comp = idx_comp + 1;
    end
            
    % Avança a janela
    if comprimiu == 1
        % Como comprimimos, já consideramos o início e o fim da janela.
        % Então descartamos o fim.
        start_window = end_window + 1;
    else
        % Quando não comprimimos, só armazenamos o início da janela, por
        % isso incrementar o início da janela.
        start_window = start_window + 1;
    end

end

% Só vamos usar até onde inserimos dados comprimidos. O resto é descartado.
comp_data = comp_data(1:idx_comp, :);

end

