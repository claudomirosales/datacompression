%% Score
% Calcula os scores dos algoritmos de compressão para cada k.
% Exibe um gráfico com todos os scores e imprime uma tabela com o ranking.

addpath('./utils/');

clc
clear all
close all

%% Variáveis auxiliares
% Separador de arquivos da plataforma.
f = filesep;

% Calcular o score desses algoritmos:
alg = {'cheb', 'pca', 'apca', 'pwlh', 'sf'};

% Quantidade de algoritmos a serem analisados.
qtdeAlg = size(alg, 2);

% Diretório onde estão os arquivos com dados brutos.
inputDir = 'results';

% Diretório de saída das imagens contendo os gráficos.
outDir = 'results/score';

% Beta da F-measure. Valores típicos: 0.5, 1, 2.
B = 2;

% Matriz para armazenar as séries de scores de todos os algoritmos.
scores = zeros(50, qtdeAlg);

% Matrix para armazenr o ranking dos algoritmos.
% (:, [NOME MAX(S(K)) K T(K) F-MEASURE E1 E2]).
ranking = cell(qtdeAlg, 7);

% Coordenadas dos menores valores de score.
x_min = inf;
y_min = inf;

for a = 1:qtdeAlg
    % Arquivo com os erros do tipo I e II.
    dados = strcat(inputDir, f, alg{a}, '_nlpca_erros.txt');
    M = tdfread(dados);
    E1 = M.Avg_T1_f_aprox;
    E2 = M.Avg_T2_f_aprox;
    
    % Arquivo com as taxas de compressão.
    dados = strcat(inputDir, f, alg{a}, '_nlpca_influencia.txt');
    M = tdfread(dados);
    
    T = M.Comp_ratio;
    P = M.Avg_pre_f_aprox;
    R = M.Avg_rec_f_aprox;
    
    % Cálculo dos scores de cada k.
    F = (1 + B^2) * ((P .* R)./((B^2 * P) + R));
    s = T + F;
    
    scores(:,a) = s;
    
    % Para os eixos do gráfico.
    [y_tmp x_tmp] = min(s);
    if y_tmp < y_min
        y_min = y_tmp;
        x_min = x_tmp;
    end
    
    % Armazena os dados.
    [y x] = max(s);
    
    ranking(a,1) = {upper(alg{a})};
    ranking(a,2) = {y};
    ranking(a,3) = {x/10};
    ranking(a,4) = {T(x, 1)};
    ranking(a,5) = {F(x, 1)};
    ranking(a,6) = {E1(x, 1)};
    ranking(a,7) = {E2(x, 1)};
    
end

% Ordena a tabela por score, em ordem decrescente, para formar o ranking.
ranking = sortrows(ranking, -2);

graf = figure;
plotStyle = {'k-','b-','r-', 'm-', 'g-'};

legendInfo = cell(size(plotStyle,2), 1);
x = 0.1:0.1:5;

for i=1:size(scores,2)
    plot(x, scores(:,i), plotStyle{i});
    legendInfo{i} = alg{i};
    hold on;
end

% Destaca o maior score
x_max = ranking{1, 3};
y_max = ranking{1, 2};

plot(x_max, y_max, 'ok', 'MarkerSize',15,'LineWidth',2);

% A escala Y fica 15% maior que os valores extremos.
ylim([y_min*0.85 y_max*1.15]);

title('Scores for compression algorithms');
xlabel('Constant{\it k} used in threshold \epsilon = k \cdot \sigma (f)');
ylabel('Score');
legend(legendInfo,'Location','southeast');

% Salva o gráfico como uma imagem PNG.
saida = strcat(outDir, f, 'scores.png');
saveas(graf, saida);
close all;

%% Imprime a tabela com o ranking.
firstLine = {'NAME', 'MAX(S(K))', 'K', 'T(K)', 'F-MEASURE', 'AVERAGE E1', 'AVERAGE E2'};
ranking(2:end+1, :) = ranking;
ranking(1, :) = firstLine;

fprintf('Tabela com o ranking dos algoritmos de compressão.\n');
disp(ranking);

