%% Compressão de Dados
% Script para comprimir os dados da ponte Z-24 utilizando um ou mais
% algoritmos de compressão. São salvos, os sinais aproximados para cada
% limiar, os tamanhos dos arquivos originais e comprimidos, o tempo de
% compressão e as features dos sinais originais e aproximados.
%

addpath('./utils/');

clc
clear all
close all

%% Carrega a série temporal

% Separador de arquivos da plataforma.
f = filesep;

% Usaremos todas as leituras feitas em uma determinada hora.
dirName = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';

% Hora do monitoramento. Valores possíveis 00-23.
hora='12';

% Padrão para buscar os arquivos de uma determinada hora.
pattern=strcat('*_',hora,'.mat');
files = dir( fullfile(dirName, pattern) );    

% Todos os arquivos .mat com as leituras da hora.
files = {files.name}';

%% Compressão dos Dados

% Quantidade de arquivos (Ex.: 200 dias/arquivos).
observ = numel(files);

% Coluna cujos dados serão comprimidos.
% COLUNA    1   2   3   4   5   6   7   8
% SENSOR    3   5   6   7   10  12  14  16
canal = 2;

ini_proc = tic;

% Algoritmos de compressão que serão usados.
%alg = {'cheb', 'pca', 'apca', 'pwlh', 'sf'};
alg = {'sf'};

% Quantidade de algoritmos.
qtdeAlg = size(alg, 2);

% Diretório onde os dados aproximados para cada limiar serão armazenados.
outputAprox = 'sinal_aproximado_normalizado';

% Cada algoritmo é executado usando vários limiares.
for a = 1:qtdeAlg
    for threshold=0.1:0.1:5.0
        % Cria o diretório deste threshold. Ex.: sinal_aproximado/pwlh/0.4
        pasta = strcat(outputAprox,f,alg{a},f,num2str(threshold));
        mkdir(pasta);

        % Matriz para guardar o tamanho dos sinais. (:, [ORIG_TAM COMP_TAM]).
        % A última linha terá o tamanho total original e o tamanho total
        % comprimido.
        save_tam = zeros(observ + 1, 2);

        % Matriz para guardar o tempo de compressão de cada sinal. A penúltima
        % linha é para incluir o tempo médio, e a última linha, o desvio-padrão.
        save_time = zeros(observ + 2, 1);

        % Feature vectors para o sinal original e aproximado.
        feat_vector_orig = zeros(observ, 3);
        feat_vector_aprx = zeros(observ, 3);

        % Comprime todas as observações para o limiar atual.
        for t=1:observ
            % Status.
            fprintf('%s: Limiar: %.2f. Comprimindo série %d de %d.\n', alg{a}, threshold, t, observ);
            % Caminho completo do arquivo .mat.
            fname = fullfile(dirName, files{t});
            % Carrega os dados como struct.
            tmp = load(fname);
            % Obtém apenas a matriz desta observação com os 8 canais.
            dataset = tmp.data;
            % Obtém as leituras do canal de interesse.
            dataset = dataset(:, canal);
            % Normaliza os dados para o intervalo [-1, 1].
            dataset = normalize_var(dataset, -1, 1);

            % Calcula o threshold para o algoritmo de compressão.
            e = threshold * std(dataset);

            % Comprime as leituras do sensor e obtém o sinal aproximado.
            if strcmp(alg{a}, 'pca') == 1
                [sinal_comp, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_pca(dataset,e);
            elseif strcmp(alg{a}, 'apca') == 1
                [sinal_comp, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_apca(dataset,e);
            elseif strcmp(alg{a}, 'pwlh') == 1
                [sinal_comp, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_pwlh(dataset,e);
            elseif strcmp(alg{a}, 'sf') == 1
                [sinal_comp, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_sf(dataset,e);
            elseif strcmp(alg{a}, 'cheb') == 1
                [sinal_comp, sinal_aprox, tam_ori, tam_comp, time_comp] = step_compression_cheb(dataset,e);
            end

            % Salva o sinal aproximado como binário. Ex.: ID_1997118_12_COL_2.bin.
            id = files{t}; % Nome do arquivo .mat que estamos comprimindo.
            arq = strcat(pasta,f,'ID_',id(5:15),'_COL_',num2str(canal),'.bin');
            salvar(arq, sinal_aprox);
            %abrir('./sinal_aproximado/ID_1997118_12_COL_2.bin',[65536,1]);

            fprintf('\tExtraindo features...\n');
            % Extrai as features do sinal original.
            feat_vector_orig(t,:) = step_extraction(dataset, 1);

            % Extrai as features do sinal aproximado.
            feat_vector_aprx(t,:) = step_extraction(sinal_aprox, 1);

            % Guarda os tamanhos do sinal original e do sinal comprimido.
            save_tam(t, 1) = tam_ori;
            save_tam(t, 2) = tam_comp;

            % Guarda o tempo gasto na compressão.
            save_time(t, 1) = time_comp;

        end

        % Salva as features do sinal original.
        arq = strcat(pasta,f,'Features_Orig_Hora_',num2str(hora),'_COL_',num2str(canal),'.bin');
        salvar(arq, feat_vector_orig);
        %FeatOriginal = abrir('sinal_aproximado/pwlh/0.1/Features_Orig_Hora_12_COL_2.bin',[234,3]);
        %234=num. de testes; 3=num. de features.

        % Salva as features do sinal aproximado.
        arq = strcat(pasta,f,'Features_Aprox_Hora_',num2str(hora),'_COL_',num2str(canal),'.bin');
        salvar(arq, feat_vector_aprx);

        % Calcula os tamanhos totais e os insere no final da matriz.
        tam_orig_total = sum(save_tam(:, 1));
        tam_comp_total = sum(save_tam(:, 2));
        save_tam(end,:) = [tam_orig_total tam_comp_total];
        % Salva os tamanhos dos sinais em um arquivo CSV.
        arq = strcat(pasta,f,'Compression_Hora_',num2str(hora),'_COL_',num2str(canal),'.csv');
        csvwrite(arq, save_tam);

        % Calcula a média e o desvio-padrão dos tempos de compressão.
        temp_media = mean(save_time(1:observ));
        temp_std = std(save_time(1:observ));
        save_time(end-1) = temp_media;
        save_time(end) = temp_std;
        % Salva os tempos em um arquivo CSV.
        arq = strcat(pasta,f,'Duracao_Hora_',num2str(hora),'_COL_',num2str(canal),'.csv');
        csvwrite(arq, save_time);

    end

end

fim = toc(ini_proc);
fprintf('Tempo total gasto: %.2f\n',fim);
timestamp();

%Fim da compressão.
