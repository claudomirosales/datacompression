function [ dataset ] = removeNan( dataset )
%REMOVENAN Remove os NaN de um vetor.
%   Procura os NaN do vetor coluna 'dataset' e os substitui pela média dos seus
%   valores vizinhos.

% Linhas e colunas onde estão os NaN.
[row, col] = find(isnan(dataset));

for r = 1:length(row)
    
    if row(r) == 1
        % Se estiver na primeira posição, será trocado pela próxima.
        dataset(row(r)) = dataset(row(r)+1);
    elseif row(r) == length(dataset)
        % Se estiver na última posição, será trocado pela anterior.
        dataset(row(r)) = dataset(row(r)-1);
    else
        % Outras posições são trocadas pela média dos valores vizinhos.
        before = dataset(row(r)-1, col(r));
        after = dataset(row(r)+1, col(r));
        dataset(row(r)) = (before + after) / 2;
    end
    
end

end

