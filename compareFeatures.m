function compareFeatures(alg, k, varargin)
%COMPAREFEATURES Plota gráficos comparando as features originais e
%aproximadas.
%
%ENTRADA
%   alg: Nome do algoritmo cujas features aproximadas serão usadas. Valores
%   possíveis são 'pca', 'apca', 'pwlh', 'cheb' e 'sf'.
%   k: Limiar de compressão do algoritmo.
%   (OPCIONAL) salvar: Se 1, salva o gráfico como imagem no diretório do
%   projeto. Se 0, não salva. Valor padrão: 0.
%   (OPCIONAL) outFile: Caminho da imagem de sáida do gráfico. Valor
%       padrão: 'results/score/best_feat_comp.png'
%   (OPCIONAL) home: Diretório onde estão as features. Valor padrão:
%       'sinal_aproximado_normalizado'.

%% Verificação dos argumentos de entrada.
% No máximo 3 argumentos opcionais.
numvarargs = length(varargin);
if numvarargs > 3
    error('Máximo de 3 argumentos opcionais permitidos.');
end

% Valores default dos argumentos opcionais.
% salvar; outFile; home.
optargs = {0 'results/score/best_feat_comp.png' 'sinal_aproximado_normalizado'};

% Sobrescreve os valores default com os especificados.
optargs(1:numvarargs) = varargin;

% Valores finais das variáveis opcionais.
[salvar, outFile, home] = optargs{:};

% Separador de arquivos da plataforma.
f = filesep;

%% Carrega as features originais e aproximadas.
arq = strcat(home, f, alg, f, num2str(k), f,'Features_Orig_Hora_12_COL_2.bin');
featuresOrig = abrir(arq,[234, 3]);

arq = strcat(home, f, alg, f, num2str(k), f,'Features_Aprox_Hora_12_COL_2.bin');
featuresAprox = abrir(arq, [234, 3]);

%% Plota os gráficos.
graf = figure;
subplot(2,1,1);
plot(featuresOrig(:,1:2), 'k');
hold on;
plot(featuresAprox(:,1:2), 'b');

t = strcat('Comparison of the original and approximated features from',...
    {' '} ,upper(alg), ' at k =', {' '}, num2str(k), '.');
title(t);
xlabel('Days');
ylabel('Amplitude');

str = '{\bf F1}';
tam = size(featuresOrig,1);
text(tam+1 , featuresOrig(tam, 1), str);

str = '{\bf F2}';
text(tam+1 , featuresOrig(tam, 2), str);

subplot(2,1,2)
plot(featuresOrig(:,3), 'k'); hold on;
plot(featuresAprox(:,3), 'b');
%legend('Original', 'Approximated', 'Location', 'Northwest');
xlabel('Days');
ylabel('Amplitude');

str = '{\bf F3}';
text(tam+1 , featuresOrig(tam, 3), str);

%% Salva o gráfico, se necessário
if salvar == 1
    saveas(graf, outFile);
    fprintf('Imagem salva em: %s\n', outFile);
    close all;
end


end
