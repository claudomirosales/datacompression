function plotPSD( alg, limiar, obsv, varargin )
%PLOTPSD Plota a PSD do sinal original e do sinal aproximado.
%   Esta função calcula a PSD de todo o sinal original de um sensor em uma
%   determinada hora. Essa PSD é comparada com a PSD do sinal aproximado
%   por um algoritmo de compressão usando um dado limiar. É importante que
%   as medições comprimidas tenham vindo do mesmo canal que está sendo
%   analisado no sinal original.
%
%ENTRADA
%   alg: Nome do algoritmo que aproximou o sinal. Valores possíveis:
%       'pca', 'apca', 'pwlh', 'cheb' e 'sf'.
%   limiar: Usar dados aproximados usando este limiar.
%   obsv: Número da observação a se calcular o PSD.
%   (OPCIONAL) salvar: Se 1, salva o gráfico como imagem no diretório do
%   projeto. Se 0, não salva. Valor padrão: 0.
%   (OPCIONAL) outFile: Caminho da imagem de sáida do gráfico. Valor
%       padrão: 'results/FFTs/total_psd_comparison.png'
%   (OPCIONAL) home: Diretório onde está o sinal aproximado. Valor padrão:
%       'sinal_aproximado_normalizado'.
%   (OPCIONAL) sensor: Usar as medições originais deste sensor. É
%   importante que a compressão tenha sido feita usando os dados deste
%   mesmo sensor. Valor padrão: 5.

addpath('./utils/');

%% Verificação dos argumentos de entrada.
% No máximo 3 argumentos opcionais.
numvarargs = length(varargin);
if numvarargs > 4
    error('Máximo de 4 argumentos opcionais permitidos.');
end

% Valores default dos argumentos opcionais.
% salvar; outFile; home.
optargs = {0 'results/FFTs/total_psd_comparison.png' 'sinal_aproximado_normalizado' 5};

% Sobrescreve os valores default com os especificados.
optargs(1:numvarargs) = varargin;

% Valores finais das variáveis opcionais.
[salvar, outFile, home, numSensor] = optargs{:};

% Separador de arquivos da plataforma.
f = filesep;

%% Mapa
% Mapa com os sensores da ponte (chaves) e as colunas da matriz (valores).
k = {3, 5, 6, 7, 10, 12, 14, 16};
v = {1, 2, 3, 4, 5, 6, 7, 8};
sensorMap = containers.Map(k, v);

% Coluna do sensor especificado.
sensor = sensorMap(numSensor);

%% Obtenção das leituras da Z24.
% Usaremos todas as leituras feitas em uma determinada hora.
dirName = '/home/afonso/Documents/UFPA/Dissertação/Data Management/DADOS/databases/Z24 Bridge/Z24Date';
hora='12'; % Hora do monitoramento. Valores possíveis 00-23.
pattern=strcat('*_',hora,'.mat'); % Padrão para buscar leituras de uma determinada hora.
files = dir( fullfile(dirName, pattern) );
files = {files.name}'; % Todos os arquivos .mat com as leituras da hora.

%% Calcula a PSD de todo o sinal original para o sensor especificado
% Caminho completo do arquivo .mat.
fname = fullfile(dirName, files{obsv});
% Carrega os dados como struct.
tmp = load(fname);
% Obtém apenas a matriz desta observação com os 8 canais.
dataset = tmp.data;
% Obtém as leituras do canal de interesse.
dataset = dataset(:, sensor);
% Normaliza os dados para o intervalo [-1, 1].
dataset = normalize_var(dataset, -1, 1);
% PSD do sinal original.
psdOriginal = fft_anpsd(dataset);

%% Obtenção das leituras aproximadas
dirName = strcat(home, f, alg, f, num2str(limiar));
files = dir( fullfile(dirName, 'ID_*') );    
files = {files.name}';
% Quantidade de medições por arquivo.
tam = 65536;

%% Calcula a PSD de todo o sinal aproximado para o algoritmmo/sensor especificado
% Caminho completo do arquivo .mat.
fname = fullfile(dirName, files{obsv});
% Obtém apenas a matriz desta observação.
dataset = abrir(fname, [tam,1]);
% PSD do sinal aproximado.
psdAproximado = fft_anpsd(dataset);


%% Plota o gráfico
graf = figure;

plot(abs(psdOriginal), 'k'); hold on;
plot(abs(psdAproximado), 'b');

t = strcat('Comparison of the original and approximated PSD from',...
    {' '} ,upper(alg), ' at k =', {' '}, num2str(limiar), '.');
title(t);
xlabel('Frequency');
ylabel('Amplitude');
legend('Original PSD', 'Approximated PSD', 'Location', 'Northwest');

%% Salva o gráfico, se necessário
if salvar == 1
    saveas(graf, outFile);
    fprintf('Imagem salva em: %s\n', outFile);
    close all;
end


end

