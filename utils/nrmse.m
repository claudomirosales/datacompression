%
% NRMSE (Normalized Root Mean Squared Error)
% 
% Moisés Felipe - Bacharelando em Ciência da Computação
% 
% Calcula o erro médio quadrático normalizado. O RMSE é um valor que usa as
% mesmas unidades que as medições. Já a versão normalizada, gera valores em
% porcentagem.
%
% INPUT:
%           orig_data: sinal original
%           aprox_data: sinal aproximado
%
% OUTPUT:
%           RMSE: valor do nrmse (1x1)
% 
%           Ex: 
%               NRMSE = nrmse(orig_data, aprox_data)
% 

function NRMSE = nrmse(orig_data, aprox_data)

if isempty(aprox_data) || isempty(orig_data)
    error('The vectors signals cant be empty');
end

if length(aprox_data) < length(orig_data)
    end_data = length(aprox_data);% Avalia por baixo
else
    end_data = length(orig_data);% Avalia por baixo
end

if isrow(orig_data)% if isn't a row vector
   error('The original signal must to be a column vector');
elseif size(orig_data,2) > 1
    orig_data = orig_data(:,2);
end

if isrow(aprox_data)% if isn't a row vector
   error('The compressed signal must to be a column vector');
elseif size(aprox_data,2) > 1
    aprox_data = aprox_data(:,2);
end


NRMSE = sum( abs(orig_data(1:end_data) - aprox_data(1:end_data)).^2 );% Soma do quadrado das diferen�as
NRMSE = sqrt(NRMSE/end_data);% Raiz quadrada da media
% Normalizing the RMSD facilitates the comparison between datasets or
% models with different scales. 
NRMSE = NRMSE/(max(orig_data(1:end_data)) - min(orig_data(1:end_data)));% Normaliza em fun��o do intervalo de dados

end
