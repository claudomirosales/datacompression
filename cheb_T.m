function output = cheb_T( i, j, N )
%CHEB_T Função auxiliar para o algoritmo de compressão Chebyshev.

output = cos( (pi * (i - 1) * ( j - 0.5 )) / N );

end

